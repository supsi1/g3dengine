/* TODO:
 *   - light and camera manager
 */

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include "scene/materials/Material.h"
#include "shading/ShaderProgramManager.h"
#include "Mesh.h"
#include "render/OvVR.h"

namespace CGLib {

    const unsigned int *Mesh::getTriangles() const {
        return m_triangles;
    }

    const glm::vec3 *Mesh::getVertices() const {
        return m_vertices;
    }

    const glm::vec3 *Mesh::getVertexInTriangle(int triangleIndex, int vertexIndex) const {
        return &m_vertices[m_triangles[triangleIndex * 3 + vertexIndex]];
    }

    void Mesh::render(glm::mat4 camera, glm::mat4 objectMatrix) {

		m_material->apply();
        ShaderProgram* program = m_material->getProgram();

        glUniformMatrix4fv(
            glGetUniformLocation(program->getId(), "projection"),
            1,
            GL_FALSE,
            glm::value_ptr(CGLib::getProjectionMatrix())
        );

        glUniformMatrix4fv(
            glGetUniformLocation(program->getId(), "modelview"),
            1,
            GL_FALSE,
            glm::value_ptr(camera * objectMatrix)
        );

        glUniformMatrix3fv(
            glGetUniformLocation(program->getId(), "normalMatrix"),
            1,
            GL_FALSE,
            glm::value_ptr(glm::inverseTranspose(glm::mat3(camera * objectMatrix)))
        );

        renderMesh();
    }

    void Mesh::renderMesh() const {
        glBindVertexArray(m_vao);

        glDrawElements(GL_TRIANGLES, m_trianglesNumber * 3, GL_UNSIGNED_INT, nullptr);

        glBindVertexArray(0);
    }

    Material *Mesh::getMaterial() {
        return m_material;
    }

    void Mesh::setMaterial(Material *material) {
        m_material = material;
    }

    const glm::vec2 *Mesh::getMapping() const {
        return m_mapping;
    }

    void Mesh::setMapping(const glm::vec2 *mMapping) {
        m_mapping = mMapping;
    }

    int Mesh::getVerticesNumber() const {
        return m_verticesNumber;
    }

    int Mesh::getFacesNumber() const {
        return m_trianglesNumber;
    }

    void Mesh::setBboxMin(glm::vec3 min)
    {
        bboxMin = min;
    }

    void Mesh::setBboxMax(glm::vec3 max)
    {
        bboxMax = max;
    }

    std::string Mesh::getType()
    {
    	return "mesh";
    }

    glm::vec3 Mesh::getBBoxMin()
    {
    	return bboxMin;
    }

	glm::vec3 Mesh::getBBoxMax()
    {
    	return bboxMax;
    }

    void Mesh::setSkipCulling(bool skip)
    {
    	m_skipCulling = skip;
    }

    bool Mesh::isSkipCulling()
    {
    	return m_skipCulling;
    }

    void Mesh::init()
    {
        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        glGenBuffers(1, &m_verticesVbo);
        glGenBuffers(1, &m_facesVbo);
        glGenBuffers(1, &m_normalsVbo);
        glGenBuffers(1, &m_mappingVbo);
        
        glBindBuffer(GL_ARRAY_BUFFER, m_verticesVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_verticesNumber, m_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_facesVbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 3 * m_trianglesNumber, m_triangles, GL_STATIC_DRAW);

        if (m_normals != nullptr) {
            glBindBuffer(GL_ARRAY_BUFFER, m_normalsVbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_verticesNumber, m_normals, GL_STATIC_DRAW);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(1);
        }

        if (m_mapping != nullptr) {
            glBindBuffer(GL_ARRAY_BUFFER, m_mappingVbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * m_verticesNumber, m_mapping, GL_STATIC_DRAW);
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
            glEnableVertexAttribArray(2);
        }

        glBindVertexArray(0);
    }

    void Mesh::deinit()
    {
        glDeleteBuffers(1, &m_verticesVbo);
        glDeleteBuffers(1, &m_mappingVbo);
        glDeleteBuffers(1, &m_normalsVbo);
        glDeleteBuffers(1, &m_facesVbo);
        glBindVertexArray(0);
        glDeleteVertexArrays(1, &m_vao);
    }

    Mesh::~Mesh() {
        delete[] m_triangles;
        delete[] m_vertices;
        delete[] m_mapping;
    }

    void Mesh::setNormals(glm::vec3 *normals) {
        this->m_normals = normals;
    }
}