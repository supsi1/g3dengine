//
// Created by ubuntu on 22/12/20.
//

#include "TextureManager.h"
#include "Texture2D.h"
#include <filtering/TrilinearFilteringStrategy.h>
#include <filtering/AnisotropicFilteringStrategy.h>
#include <filtering/BilinearFilteringStrategy.h>
#include <filtering/NoFilteringStrategy.h>


namespace CGLib {
    TextureManager &TextureManager::getInstance() {

        static TextureManager instance{};

        return instance;

    }

    void TextureManager::forceMassiveReload() const {
        for (std::pair < int, Texture * > element : m_texture_map) {
            element.second->clean();
            element.second->loadToGPU();
        }
    }

    Texture* TextureManager::getDefault()
    {
        if (m_default == nullptr) {
            unsigned char* bitmap = new unsigned char[256 * 256 * 4];
            for (int i = 0; i < 256 * 256 * 4; i++)
                bitmap[i] = 255;

            m_default = new Texture2D();
            m_default->setBitmap(bitmap);
            m_default->setWidth(256);
            m_default->setHeight(256);
            m_default->loadToGPU();
        }
        return m_default;
    }


    void TextureManager::addTexture(unsigned int id, Texture *texture) {
        m_texture_map.emplace(id, texture);
    }

    void TextureManager::removeTexture(unsigned int id) {
        m_texture_map.erase(id);
    }

    Texture *TextureManager::getTexture(unsigned int id) const {
        return m_texture_map.at(id);
    }

    void TextureManager::setFilteringTemplate(FilteringTemplate *filteringTemplate) {
        m_filtering = filteringTemplate;
    }

    FilteringTemplate *TextureManager::getFilteringTemplate() const {
        return m_filtering;
    }

    TextureManager::TextureManager() {
        this->m_filtering = new FilteringTemplate();
        m_filtering->setPerPixelStrategy(new NoFilteringStrategy());

    }
}