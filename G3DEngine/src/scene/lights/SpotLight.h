#pragma once

#include "Defs.h"

#include "Light.h"

namespace CGLib {
    class LIB_API SpotLight : public Light {

    private:
        float m_cutoff;
        glm::vec3 m_direction;

    public:

        explicit SpotLight(unsigned int lightRef = 0,
                           const glm::vec3 &rotate = glm::vec3{0.0f},
                           const glm::vec3 &translate = glm::vec3{0.0f},
                           const glm::vec3 &scale = glm::vec3{1.0f},
                           const glm::vec4 &diffuse = glm::vec4{1.0f},
                           const glm::vec4 &ambient = glm::vec4{1.0f},
                           const glm::vec4 &specular = glm::vec4{1.0f},
                           float linear = 0,
                           float constant = 1,
                           float quadratic = 0,
                           float cutoff = 45.0f,
                           glm::vec3 direction = glm::vec3{0.0f});

        [[nodiscard]] glm::vec3 getDirection() const;

        [[nodiscard]] float getCutoff() const;

        void setDirection(const glm::vec3 &m_direction);

        void setCutoff(float m_cutoff);

        void apply() override;

    };
}
