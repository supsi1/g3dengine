#include "IndirectRenderingList.h"

#include "logging/Log.h"
#include "camera/CameraManager.h"
#include "scene/Mesh.h"

namespace CGLib
{
	void IndirectRenderingList::clear()
	{
		elements.clear();
	}

	void IndirectRenderingList::pass(SceneNode* node, glm::mat4 matrix)
	{
		glm::mat4 updatedMatrix = matrix * node->getTotalTransform();

		add(node, updatedMatrix);

		std::vector<SceneNode*> children = node->getChildren();
		if (!children.empty())
			for (auto& element : children)
			{
				pass(element, updatedMatrix);
			}
	}

	void IndirectRenderingList::add(SceneNode* node, glm::mat4 matrix)
	{
		if (node->getType() == "light")
		{
			elements.push_front(std::pair<SceneNode*, glm::mat4>{node, matrix});
		}
		else if (node->getType() == "mesh")
		{
			elements.push_back(std::pair<SceneNode*, glm::mat4>{node, matrix});
		}
	}

	void IndirectRenderingList::render(glm::mat4 cameraMatrix)
	{
		getCtxStatus()->renderedMeshes = 0;
		for (auto& element : elements)
		{
			if (element.first->getType() == "mesh" && !static_cast<Mesh*>(element.first)->isSkipCulling())
			{
				
				bool insideFrustum = false;
				Mesh* mesh = static_cast<Mesh*>(element.first);
				glm::mat4 transform = element.second;

				
				glm::vec3 bMin = transform * glm::vec4(mesh->getBBoxMin(), 1);
				glm::vec3 bMax = transform * glm::vec4(mesh->getBBoxMax(), 1);

				glm::vec3 xLen = glm::vec3(1, 0, 0) * (bMax.x - bMin.x);
				glm::vec3 yLen = glm::vec3(0, 1, 0) * (bMax.y - bMin.y);
				glm::vec3 zLen = glm::vec3(0, 0, 1) * (bMax.z - bMin.z);

				std::vector<glm::vec3> points;
				
				points.push_back(bMin);
				points.push_back(bMax);
				points.push_back(bMin + zLen);
				points.push_back(bMin + xLen);
				points.push_back(bMin + yLen);
				points.push_back(bMax - zLen);
				points.push_back(bMax - xLen);
				points.push_back(bMax - yLen);

				for (auto& camera : CameraManager::getInstance().getCameras())
				{
					if(!camera->getFrustum().isEnabled())
						continue;

					auto eye = camera->getEye();
					
					for(auto& p: points)
						if(camera->getFrustum().isInside(p))
						{
							insideFrustum = true;
							break;
						}
				
					if(insideFrustum) break;
				}

				if(insideFrustum)
				{
					element.first->render(cameraMatrix, element.second);
					++getCtxStatus()->renderedMeshes;
				}
			}else{
				element.first->render(cameraMatrix, element.second);
			}
		}
	}
}
