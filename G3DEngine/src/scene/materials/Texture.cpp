#include "Texture.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <scene/materials/TextureManager.h>

namespace CGLib {
    const unsigned int Texture::getTexId() const {
        return m_tex_id;
    }

    unsigned char* Texture::getBitmap() const {
        return m_bitmap;
    }

    void Texture::setBitmap(unsigned char* mBitmap) {
        m_bitmap = mBitmap;
    }

    unsigned int Texture::getWidth() const {
        return m_width;
    }

    void Texture::setWidth(unsigned int mWidth) {
        m_width = mWidth;
    }

    unsigned int Texture::getHeight() const {
        return m_height;
    }

    void Texture::setHeight(unsigned int mHeight) {
        m_height = mHeight;
    }

    void Texture::clean() {
        glDeleteTextures(1, &m_tex_id);
    }

    Texture::~Texture() {
        clean();
    }
    void Texture::loadToGPU()
    {
        glGenTextures(1, &m_tex_id);
        load();
    }
}