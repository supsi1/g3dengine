#pragma once
#include <Defs.h>

namespace CGLib {
	class LIB_API App
	{
	public:
		App();
		virtual ~App();
		virtual void onInit();
		virtual void onInitComplete();
		virtual void onRender();
		virtual void onClose();

		virtual void onKeyboardInput(unsigned char key, int mouseX, int mouseY);
		virtual void onResize(int width, int height);
	};

	App* createCGApp();
}
