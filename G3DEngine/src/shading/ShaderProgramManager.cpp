#include <GL/glew.h>
#include "ShaderProgramManager.h"
#include <string>
#include <glm/gtc/type_ptr.hpp>
#include <scene\lights\LightManager.h>


CGLib::ShaderProgramManager::ShaderProgramManager()
{
	 static char vertexSource[] = R"(										
		#version 440 core

		uniform mat4 projection;
		uniform mat4 modelview;
		uniform mat3 normalMatrix;

		layout(location=0) in vec3 in_vertex;
		layout(location=1) in vec3 in_Normal;
		layout(location = 2) in vec2 in_TexCoord;

		out vec4 fragPosition;
		out vec3 normal; 
		out vec2 texCoord;

		void main(void) {
		   texCoord = in_TexCoord;
			fragPosition = modelview * vec4(in_vertex, 1.0f);
			gl_Position = projection * fragPosition;
			normal = normalMatrix * in_Normal; 
		}
	)";

	 static char fragmentSource[] = R"(										
	   #version 440 core													
								
	   #define LIGHTS 8
												
	   in vec4 fragPosition;
	   in vec3 normal;   
      in vec2 texCoord;
   
	   out vec4 fragOutput;

		//Texture Mapping
	   layout(binding = 0) uniform sampler2D texSampler;

	   // Material properties:
	   uniform vec3 matEmission;
	   uniform vec3 matAmbient;
	   uniform vec3 matDiffuse;
	   uniform vec3 matSpecular;
	   uniform float matShininess;

	   // Light properties:
		uniform float enabled[LIGHTS];
	   uniform vec3 lightPosition[LIGHTS]; 
	   uniform vec3 lightAmbient; 
	   uniform vec3 lightDiffuse[LIGHTS]; 
	   uniform vec3 lightSpecular[LIGHTS];

	   void main(void)
	   {      
		 vec4 texel = texture(texSampler, texCoord);

		  // Ambient term:
		  vec3 fragColor = matEmission + matAmbient * lightAmbient;
		  for(int i = 0; i< LIGHTS; i++){
				if(enabled[i] == 0) continue;
				// Diffuse term:
				vec3 _normal = normalize(normal);
				vec3 lightDirection = normalize(lightPosition[i] - fragPosition.xyz);      
				float nDotL = dot(lightDirection, _normal);
				if (nDotL > 0.0f)
				{
				  fragColor += matDiffuse * nDotL * lightDiffuse[i];
      
				  // Specular term:
				  vec3 halfVector = normalize(lightDirection + normalize(-fragPosition.xyz));                     
				  float nDotHV = dot(_normal, halfVector);         
				  fragColor += matSpecular * pow(nDotHV, matShininess) * lightSpecular[i];
				} 
		  }

		 
      
		  // Final color:
		 fragOutput = texel * vec4(fragColor, 1.0f);
	   }																		
	)";

	 m_defaultVertexShader = new ShaderObject(vertexSource, ShaderType::VERTEX);
	 m_defaultFragmentShader = new ShaderObject(fragmentSource, ShaderType::FRAGMENT);
}

CGLib::ShaderProgramManager &CGLib::ShaderProgramManager::getInstance()
{
	static ShaderProgramManager instance;

	return instance;

}

void CGLib::ShaderProgramManager::addProgram(ShaderProgram* program)
{
	
	m_programs.push_back(program);

}

CGLib::ShaderProgram* CGLib::ShaderProgramManager::getDefaultProgram() 
{
	return &m_defaultProgram;
}


void CGLib::ShaderProgramManager::useProgram(ShaderProgram* program)
{
	glUseProgram(program->getId());
}

void CGLib::ShaderProgramManager::setDefaultFragmentShader(ShaderObject* fragmentShader)
{
	 if (fragmentShader->getType() != ShaderType::FRAGMENT) return;
	 m_defaultFragmentShader = fragmentShader;
	 m_defaultProgram.setFragmentShader(m_defaultFragmentShader);
}

void CGLib::ShaderProgramManager::setLight(int ref, glm::vec4 position, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 ambient)
{

	 for (auto* program : m_programs) {
		  int id = program->getId();
		  useProgram(program);
		  glUniform3fv(
				glGetUniformLocation(id, ("lightPosition[" + std::to_string(ref) + "]").c_str()),
				1,
				glm::value_ptr(position)
		  );

		  glUniform3fv(
				glGetUniformLocation(id, ("lightDiffuse[" + std::to_string(ref) + "]").c_str()),
				1,
				glm::value_ptr(diffuse)
		  );
		  glUniform3fv(
				glGetUniformLocation(id, ("lightSpecular[" + std::to_string(ref) + "]").c_str()),
				1,
				glm::value_ptr(specular)
		  );

		  glUniform3fv(
				glGetUniformLocation(id, "lightAmbient"),
				1,				
				glm::value_ptr(LightManager::getInstance().getGlobalAmbient())
		  );

		  glUniform1f(
				glGetUniformLocation(id, ("enabled[" + std::to_string(ref) + "]").c_str()),
				1.0
		  );
	 }
}

void CGLib::ShaderProgramManager::enableLight(int ref, bool enabled)
{
	 for (auto* program : m_programs) {
		  int id = program->getId();
		  glUniform1f(
				glGetUniformLocation(id, ("enabled[" + std::to_string(ref) + "]").c_str()),
				enabled ? 1.0 : 0.0
		  );
	 }
}

void CGLib::ShaderProgramManager::init()
{
	m_defaultProgram = ShaderProgram{};
	m_defaultProgram.setVertexShader(m_defaultVertexShader);
	m_defaultProgram.setFragmentShader(m_defaultFragmentShader);
	m_defaultProgram.createProgram();

	for (auto& program : m_programs) {
		if (program->m_fragmentShader == nullptr) {
			program->m_fragmentShader = m_defaultFragmentShader;
		}

		if (program->m_vertexShader == nullptr) {
			program->m_vertexShader = m_defaultVertexShader;
		}

		program->createProgram();
	}

	addProgram(getDefaultProgram());

}

void CGLib::ShaderProgramManager::deinit()
{

	m_defaultProgram.deleteProgram();

	for (auto& program : m_programs)
		program->deleteProgram();

}


