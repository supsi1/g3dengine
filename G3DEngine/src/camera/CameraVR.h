#pragma  once
#include "Camera.h"
#include "Defs.h"

namespace CGLib {

	class LIB_API CameraVR : public Camera {

	private:
		Camera *m_left;
		Camera *m_right;
		
	public:
		~CameraVR();
		
		explicit CameraVR(
			const glm::vec3& rotate = glm::vec3{ 0.0f },
			const glm::vec3& translate = glm::vec3{ 0.0f },
			const glm::vec3& scale = glm::vec3{ 1.0f },
			const glm::vec3& eye = glm::vec3{ 0.0f, 0.0f, 0.0f },
			const glm::vec3& center = glm::vec3{ 0.0f, 0.0f, -1.0f },
			const glm::vec3& up = glm::vec3{ 0.0f, 1.0f, 0.0f });
		                                    
		void computeFrustum() override;

		void setHead(const glm::vec3& mHead);

		void translate(glm::vec3 translation) override;


		glm::vec3 getForward() const override;
		glm::mat4 getLeftEyeMatrix() const;
		glm::mat4 getRightEyeMatrix() const;
		const glm::vec3 getEye() const override;
		static glm::mat4 getRightEyeProjectionMatrix();
		static glm::mat4 getLeftEyeProjectionMatrix();

	};


}

