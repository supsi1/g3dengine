#pragma once

#include "Defs.h"

namespace CGLib {
    class LIB_API IStrategy {
    public:
        virtual void execute() = 0;

        virtual ~IStrategy() {}
    };
}