#pragma once

#include "../src/cglib.h"
#include "../src/IStrategy.h"
#include "../src/ITemplate.h"
#include "../src/transform.h"
#include "../src/logging/Log.h"
#include "../src/App.h"
#include "../src/GUI/GUIManager.h"

#include "../src/camera/Camera.h"
#include "../src/camera/Frustum.h"
#include "../src/camera/CameraManager.h"
#include "../src/camera/CameraVR.h"
#include "../src/filtering/AnisotropicFilteringStrategy.h"
#include "../src/filtering/BilinearFilteringStrategy.h"
#include "../src/filtering/FilteringTemplate.h"
#include "../src/filtering/LinearFilteringStrategy.h"
#include "../src/filtering/NoFilteringStrategy.h"
#include "../src/filtering/TrilinearFilteringStrategy.h"
#include "../src/parser/FileParser.h"
#include "../src/parser/OVOStrategy.h"
#include "../src/parser/ParsingStrategy.h"

#include "../src/scene/lights/DirectionalLight.h"
#include "../src/scene/lights/Light.h"
#include "../src/scene/lights/LightBuilder.h"
#include "../src/scene/lights/LightManager.h"
#include "../src/scene/lights/OmniDirectionalLight.h"
#include "../src/scene/lights/Spotlight.h"

#include "../src/scene/materials/Material.h"
#include "../src/scene/materials/MaterialManager.h"
#include "../src/scene/materials/Texture.h"
#include "../src/scene/materials/Texture2D.h"
#include "../src/scene/materials/TextureManager.h"
#include "../src/scene/materials/Texture3D.h"

#include "../src/scene/EmptyNode.h"
#include "../src/scene/Mesh.h"
#include "../src/scene/SceneGraph.h"
#include "../src/scene/SceneNode.h"
#include "../src/scene/SceneObject.h"

#include "../src/shading/ShaderObject.h"
#include "../src/shading/ShaderProgram.h"
#include "../src/shading/ShaderProgramManager.h"

#include "../src/scene/render/IndirectRendering.h"
#include "../src/scene/render/IndirectRenderingList.h"
#include "../src/scene/render/IRenderingStrategy.h"
#include "../src/scene/render/StereoscopicRendering.h"

#include "../src/scene/Plane.h"
#include "../src/scene/Skybox.h"

#include "../src/EntryPoint.h"