#include "ShaderObject.h"
#include "ShaderObject.h"
#include <GL/glew.h>
#include <logging/Log.h>

CGLib::ShaderObject::ShaderObject(char* source, ShaderType type) 
	: m_source{ source }, m_type{ type }{}

CGLib::ShaderObject::ShaderObject(FILE* shaderFile, ShaderType type) : m_type{type}
{
	fseek(shaderFile, 0, SEEK_END);
	long fsize = ftell(shaderFile);
	fseek(shaderFile, 0, SEEK_SET);
	
	m_source = new char[fsize];
	fsize = fread(m_source, sizeof(char), fsize, shaderFile);
	m_source[fsize] = '\0';
}

unsigned int CGLib::ShaderObject::getId() const
{
	return m_id;
}

CGLib::ShaderType CGLib::ShaderObject::getType() const
{
	return m_type;
}

void CGLib::ShaderObject::createShader()
{
	if (glIsShader(m_id) == GL_TRUE) {
		CG_CR_LOG_INFO("Shader id {} has already been created", m_id);
		return;
	}

	switch (m_type)
	{
	case CGLib::VERTEX:
		m_id = glCreateShader(GL_VERTEX_SHADER);
		break;
	case CGLib::FRAGMENT:
		m_id = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case CGLib::GEOMETRY:
		m_id = glCreateShader(GL_GEOMETRY_SHADER);
		break;
	case CGLib::COMPUTE:
		m_id = glCreateShader(GL_COMPUTE_SHADER);
		break;
	default:
		CG_CR_LOG_ERR("Undefined shader type {}", m_type);
		return;
	}

	if (m_id == 0) {
		CG_CR_LOG_ERR("Shader has failed creation");
		return;
	}

	glShaderSource(m_id, 1, &m_source, nullptr);

	CG_CR_LOG_INFO("Shader id {} generated and bound to source", m_id);

}

void CGLib::ShaderObject::compileShader() const
{
	CG_CR_LOG_INFO("Compiling shader id {}", m_id);

	GLint isCompiled;
	glGetShaderiv(m_id, GL_COMPILE_STATUS, &isCompiled);

	if (isCompiled == GL_TRUE) {
		CG_CR_LOG_INFO("Shader id {} has already been compiled", m_id);
		return;
	}

	glCompileShader(m_id);

	glGetShaderiv(m_id, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE) {
		CG_CR_LOG_ERR("Shader id {} compilation error", m_id);
		displayShaderInfo();
		return;
	}

	CG_CR_LOG_INFO("Shader id {} compiled", m_id);

}

void CGLib::ShaderObject::displayShaderInfo() const {
	int infoLenght;
	glGetShaderiv(m_id, GL_INFO_LOG_LENGTH, &infoLenght);
	if (infoLenght == 0) {
		CG_LOG_INFO("No info for shader id {}", m_id);
		return;
	}

	char *logs = new char[infoLenght];

	glGetShaderInfoLog(m_id, infoLenght, NULL, logs);
	CG_CR_LOG_INFO("Shader info for id {}:\n{}", m_id, logs);

	delete[] logs;
}

inline void CGLib::ShaderObject::deleteShader()
{
	glDeleteShader(m_id);
}


