#pragma once
#include "Defs.h"
#include "Light.h"

namespace CGLib {

    class LIB_API DirectionalLight : public Light {

    private:
        glm::vec3 m_direction;

    public:

        explicit DirectionalLight(unsigned int light_ref = 0,
            const glm::vec3& rotate = glm::vec3{ 0.0f },
            const glm::vec3& translate = glm::vec3{ 0.0f },
            const glm::vec3& scale = glm::vec3{ 1.0f },
            const glm::vec4& diffuse = glm::vec4{ 1.0f },
            const glm::vec4& ambient = glm::vec4{ 1.0f },
            const glm::vec4& specular = glm::vec4{ 1.0f },
            float linear = 0,
            float constant = 1,
            float quadratic = 0);

        void apply() override;

        [[nodiscard]] glm::vec4 getPosition() const override;

        void setDirection(const glm::vec3& m_direction);

    };
}
