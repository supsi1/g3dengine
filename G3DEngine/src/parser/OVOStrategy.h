#pragma once

#include "ParsingStrategy.h"
#include "Defs.h"
#include "../scene/SceneNode.h"
namespace CGLib {
    class LIB_API OVOStrategy : public ParsingStrategy {
    private:
        virtual SceneNode *parse(FILE *fp);

    public:
        OVOStrategy() = default;;

        void execute() override;
    };
}