//
// Created by kalu on 12/25/20.
//

#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>
#include "AnisotropicFilteringStrategy.h"

namespace CGLib {

    void AnisotropicFilteringStrategy::execute() {

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, m_multiplier);
    }

    void AnisotropicFilteringStrategy::setMultiplier(int value) {
        this->m_multiplier = value;
    }
}