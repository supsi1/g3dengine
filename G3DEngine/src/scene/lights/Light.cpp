//
// Created by purpleknight on 11/16/20.
//

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "scene/SceneNode.h"
#include "scene/LuminousObject.h"
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include "cglib.h"
#include "shading/ShaderProgramManager.h"

#include "Light.h"

namespace CGLib {
    void Light::apply() {
		int ref = getLightRef() - CGLIB_LIGHT0;

		ShaderProgramManager::getInstance().setLight(ref, lastTransform * getPosition(), getDiffuse(), getSpecular(), getAmbient());

    }

    glm::vec4 Light::getPosition() const {
        glm::vec4 vec{0.0f, 0.0f, 0.0f, 1.0f};
        vec.w = m_position;
        return vec;
    }

    void Light::setPosition(const float position) {
        Light::m_position = position;
    }

    float Light::getLinearAttenuation() const {
        return m_linear;
    }

    void Light::setLinearAttenuation(float linear) {
        Light::m_linear = linear;
    }

    float Light::getConstantAttenuation() const {
        return m_constant;
    }

    void Light::setConstantAttenuation(float constant) {
        Light::m_constant = constant;
    }

    float Light::getQuadraticAttenuation() const {
        return m_quadratic;
    }

    void Light::setQuadraticAttenuation(float quadratic) {
        Light::m_quadratic = quadratic;
    }

    void Light::setInfluence(float radius) {
        this->setConstantAttenuation(1.0f);
        radius = radius * 10;
        this->setQuadraticAttenuation(1.0f / radius);
        this->setLinearAttenuation(-getQuadraticAttenuation() * radius / 2.0f - 1.0f / radius);
    }

    Light::Light(unsigned int light_ref,
                 const glm::vec3 &rotate,
                 const glm::vec3 &translate,
                 const glm::vec3 &scale,
                 const glm::vec4 &diffuse,
                 const glm::vec4 &ambient,
                 const glm::vec4 &specular,
                 float position,
                 float linear,
                 float constant,
                 float quadratic)
            : SceneNode(rotate, translate, scale),
              LuminousObject(diffuse, ambient, specular),
              m_light_ref(light_ref),
              m_position(position),
              m_linear(linear),
              m_constant(constant),
              m_quadratic(quadratic) {
        setId(getId().append("light"));
    }


    unsigned int Light::getLightRef() const {
        return m_light_ref;
    }

    std::string Light::getType()
    {
    	return "light";
    }

    void Light::enable() const {
		  ShaderProgramManager::getInstance().enableLight(getLightRef() - CGLIB_LIGHT0, true);
    }

    void Light::disable() const {
		  ShaderProgramManager::getInstance().enableLight(getLightRef() - CGLIB_LIGHT0, false);
    }

    void Light::render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) {
        lastTransform = cameraMatrix * objectMatrix;
        if (getLightRef() != 0)
            apply();
    }
    void Light::init()
    {
    }
    void Light::deinit()
    {
    }
}
