#pragma once

#include "Defs.h"

#include "../SceneNode.h"
#include "../LuminousObject.h"

namespace CGLib {

    class LIB_API Light : public SceneNode, public LuminousObject {

    private:

        unsigned int m_light_ref;
        float m_linear, m_constant, m_quadratic;

        friend void setLightRef(Light *light, unsigned int lightRef);

    protected:
       float m_position;
       glm::mat4 lastTransform;
    public:

        explicit Light(unsigned int light_ref = 0,
                       const glm::vec3 &rotate = glm::vec3{0.0f},
                       const glm::vec3 &translate = glm::vec3{0.0f},
                       const glm::vec3 &scale = glm::vec3{1.0f},
                       const glm::vec4 &diffuse = glm::vec4{0.5f},
                       const glm::vec4 &ambient = glm::vec4{0.0f},
                       const glm::vec4 &specular = glm::vec4{1.0f},
                       float position = 0.0f,
                       float linear = 0,
                       float constant = 1,
                       float quadratic = 0);

        [[nodiscard]] virtual glm::vec4 getPosition() const;

        void setPosition(float position);

        [[nodiscard]] float getLinearAttenuation() const;

        void setLinearAttenuation(float linear);

        [[nodiscard]] float getConstantAttenuation() const;

        void setConstantAttenuation(float constant);

        [[nodiscard]] float getQuadraticAttenuation() const;

        void setQuadraticAttenuation(float quadratic);

        [[nodiscard]] unsigned int getLightRef() const;


        std::string getType() override;
    	
        void setInfluence(float radius);

        void enable() const;

        void disable() const;

        void apply() override;

        void render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) override;

        void init() override;
        void deinit() override;
    };

}
