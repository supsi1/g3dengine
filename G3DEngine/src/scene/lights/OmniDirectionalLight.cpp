//
// Created by ubuntu on 29/11/20.
//

#include "OmniDirectionalLight.h"
#include <GL/glew.h>
#include <cglib.h>
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>

namespace CGLib {
    void OmniDirectionalLight::apply() {
        //glLightfv(getLightRef(), CGLIB_SPOT_CUTOFF, &m_cutoff);

        Light::apply();

    }

    OmniDirectionalLight::OmniDirectionalLight(unsigned int lightRef,
                                               const glm::vec3 &rotate,
                                               const glm::vec3 &translate,
                                               const glm::vec3 &scale,
                                               const glm::vec4 &diffuse,
                                               const glm::vec4 &ambient,
                                               const glm::vec4 &specular,
                                               float linear,
                                               float constant,
                                               float quadratic)
            : Light(lightRef, rotate, translate, scale, diffuse, ambient, specular, 1.0f, linear, constant, quadratic),
              m_cutoff{180.0f} {}

    float OmniDirectionalLight::getCutoff() const {
        return m_cutoff;
    }
}