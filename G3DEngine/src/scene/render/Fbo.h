#pragma once

#include "Defs.h"

namespace CGLib {
	class Texture2D;

	class LIB_API Fbo
	{
	private:

		unsigned int m_id;
		unsigned int m_dbufferId;
		unsigned int m_texId;
		unsigned int m_x;
		unsigned int m_y;


	public:

		Fbo() = default;

		void bindColorTexture(Texture2D *texture);

		void bindDepthBuffer(unsigned int x, unsigned int y);

		void bind();

		static void unbind();

		void init();

		void deinit();


		unsigned getDepthBufferId() const
		{
			return m_dbufferId;
		}

		unsigned getTextureId() const
		{
			return m_texId;
		}

		unsigned getId() const
		{
			return m_id;
		}
	};
}
