// GLM:
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"
#include <glm/gtc/type_ptr.hpp>

// C/C++:
#include <iostream>

// Logging
#include <spdlog/spdlog.h>

// Engine
#include <CGEngine.h>

// Disable/Enable VR by commenting/uncommenting this define
#define VR

class SupSaber : public CGLib::App
{
private:

	CGLib::IndirectRenderingList* renderList;
	CGLib::StereoscopicRendering* renderStrategy;


	float openStep = 0.01f;
	float totalDelta = 1;
	float currentDelta = 0;
	std::string status;
	CGLib::SceneNode* ray;
	CGLib::SceneNode* rayParent;

#ifdef VR
	CGLib::CameraVR* m_camera;
#else
	CGLib::Camera* m_camera;
#endif // VR

public:
	SupSaber()
	{
	}

	~SupSaber()
	{
	}

	void onInitComplete() override
	{
		//create camera
#ifdef VR
		m_camera = new CGLib::CameraVR{};
#else
		m_camera = new CGLib::Camera{};
#endif

		m_camera->setCenter({0, 0, -1});
		//m_camera->translate(glm::vec3{0.0f, 0, 0});
		CGLib::CameraManager::getInstance().add((CGLib::Camera*)m_camera);
		CGLib::CameraManager::getInstance().enable((CGLib::Camera*)m_camera);
		CG_LOG_INFO("added camera");


		CGLib::setAssetsDirPath("assets");
		CGLib::Light* moonlight = CGLib::LightBuilder::getInstance()
		                          .begin()
		                          .setLinearAttenuation(0.1f)
		                          .setConstantAttenuation(0)
		                          .setQuadraticAttenuation(0.2f)
		                          .buildOmniDirectional();

		CGLib::Light* light2 = CGLib::LightBuilder::getInstance()
		                       .begin()
		                       .setLinearAttenuation(0.1f)
		                       .setConstantAttenuation(0)
		                       .setQuadraticAttenuation(0.2f)
		                       .buildOmniDirectional();

		moonlight->setLocalTranslation(glm::vec3{-10, 10, -10});
		light2->setLocalTranslation(glm::vec3{0, 0, 0});

		CGLib::LightManager::getInstance().add(moonlight);
		CGLib::LightManager::getInstance().add(light2);
		CGLib::LightManager::getInstance().enable(moonlight);
		CGLib::LightManager::getInstance().enable(light2);

		//load scene
		std::string fileName = "scene.OVO";
		CGLib::FileParser fp{};
		fp.setStrategy(new CGLib::OVOStrategy());
		CGLib::SceneNode* loadedScene = fp.loadFromFile(fileName, true);


		float scaleSkybox = 500.0f;
		std::string images[6];
		for (int i = 1; i <= 6; i++)
		{
			images[i - 1] = (std::string(CGLib::getAssetsDirPath()) + separator() + std::string("skybox") +
				std::to_string(i) + ".png");
		}

		CGLib::Skybox* skyBox = new CGLib::Skybox(images);
		skyBox->setLocalScale(glm::vec3{scaleSkybox, scaleSkybox, scaleSkybox});
		skyBox->translate(glm::vec3{0.0f, 1.0f, 0.0f});
		skyBox->setSkipCulling(true);
		loadedScene->translate({0, -100, 0});
		loadedScene->addChild(skyBox);

		CGLib::SceneGraph::getInstance().addRootNode(loadedScene);

		//Initial parameters
		status = "CLOSED";
		rayParent = CGLib::SceneGraph::getInstance().findNode("ray")->getParent();
		ray = CGLib::SceneGraph::getInstance().popNode("ray");
		ray->init();

		CGLib::Mesh* terrain = static_cast<CGLib::Mesh*>(loadedScene->findNodeInChildren("Terrain"));
		terrain->setSkipCulling(true);

		renderList = new CGLib::IndirectRenderingList();
		renderStrategy = new CGLib::StereoscopicRendering(renderList);

		renderStrategy->setLensWidth(CGLib::getCtxStatus()->windowWidth / 2);
		renderStrategy->setLensHeight(CGLib::getCtxStatus()->windowHeight);


		auto* indirectRenderStrategy = new CGLib::IndirectRendering(renderList);
#ifdef VR
		CGLib::SceneGraph::getInstance().setRenderingStrategy(renderStrategy);
#else
		CGLib::SceneGraph::getInstance().setRenderingStrategy(indirectRenderStrategy);
#endif
	}

	void onRender() override
	{
		CGLib::SceneNode* leftCap = CGLib::SceneGraph::getInstance().findNode("obs_left_caps");
		CGLib::SceneNode* rightCap = CGLib::SceneGraph::getInstance().findNode("obs_right_caps");

		if (status == "OPENING")
		{
			leftCap->translate(glm::vec3{0, 0, openStep});
			rightCap->translate(glm::vec3{0, 0, openStep});
			currentDelta += openStep;

			if (currentDelta >= totalDelta)
			{
				status = "OPEN";
				rayParent->addChild(ray);
			}
		}

		if (status == "CLOSING")
		{
			leftCap->translate(glm::vec3{0, 0, -openStep});
			rightCap->translate(glm::vec3{0, 0, -openStep});
			currentDelta -= openStep;

			if (currentDelta <= 0)
			{
				status = "CLOSED";
				rayParent = CGLib::SceneGraph::getInstance().findNode("ray")->getParent();
				ray = CGLib::SceneGraph::getInstance().popNode("ray");
			}
		}

		CGLib::SceneGraph::getInstance().renderScene(CGLib::CameraManager::getInstance().getActiveCamera());
	}

	void onResize(int width, int height) override
	{
		(renderStrategy)->setLensHeight(height);
		(renderStrategy)->setLensWidth(width / 2);
	}

	void onClose() override
	{
		delete renderList;
		delete renderStrategy;
	}

	void onKeyboardInput(unsigned char key, int mouseX, int mouseY) override
	{
		switch (key)
		{
		case 'w':
			m_camera->translate(m_camera->getForward());
			break;
		case 's':
			m_camera->translate(-m_camera->getForward());
			break;
		case ' ':
			if (status == "CLOSED")
			{
				status = "OPENING";
			}
			else if (status == "OPEN")
			{
				status = "CLOSING";
			}
			else if (status == "OPENING")
			{
				status = "CLOSING";
			}
			else if (status == "CLOSING")
			{
				status = "OPENING";
			}
			break;
#ifndef VR
		case 'e':
			m_camera->rotate(glm::vec3{0, -1, 0});
			break;
		case 'q':
			m_camera->rotate(glm::vec3{0, 1, 0});
			break;
#endif
		}
	}
};

CGLib::App* CGLib::createCGApp()
{
	return new SupSaber();
}
