#pragma once

#include "Defs.h"
#include "../scene/SceneNode.h"
#include "../camera/Frustum.h"

namespace CGLib {
    class LIB_API Camera : public SceneNode {
    protected:
        glm::vec3 m_eye;
        glm::vec3 m_center;
        glm::vec3 m_up;
        Frustum m_frustum;
    	bool m_dirty;
    
    public:
        explicit Camera(
                const glm::vec3 &rotate = glm::vec3{0.0f},
                const glm::vec3 &translate = glm::vec3{0.0f},
                const glm::vec3 &scale = glm::vec3{0.0f},
                const glm::vec3 &eye = glm::vec3{0.0f, 0.0f, 0.0f},
                const glm::vec3 &center = glm::vec3{0.0f, 0.0f, -1.0f},
                const glm::vec3 &up = glm::vec3{0.0f, 1.0f, 0.0f});
    
        [[nodiscard]] glm::mat4 getCamera() const;
    
        [[nodiscard]]virtual const glm::vec3 getEye() const;
    
        [[nodiscard]] const glm::vec3 &getCenter() const;
    
        [[nodiscard]] const glm::vec3 &getUp() const;
    
        virtual void setEye(const glm::vec3 &mEye);
    
        virtual void setCenter(const glm::vec3 &mCenter);
    
        virtual void setUp(const glm::vec3 &mUp);
    
        virtual void rotate(glm::vec3 eulerAngles) override;
    
        virtual void translate(glm::vec3 translation) override;
    
        virtual void lookTo(glm::vec3 direction, glm::vec3 up);
    
        void render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) override;

        void setFrustum(Frustum f);
    	
        virtual void computeFrustum();

    	virtual glm::vec3 getForward() const;

        std::string getType() override;
    	
    	Frustum& getFrustum();

    	bool isDirty();

        void init() override;
        void deinit() override;
    };
}

