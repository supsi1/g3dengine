#pragma once

#include "Camera.h"

#include "Defs.h"

namespace CGLib {
    class LIB_API CameraManager {
    private:
        Camera* m_default_camera;
        Camera* m_active_camera;
        std::vector<Camera*> m_cameras;

        CameraManager();

    public:
        static CameraManager& getInstance();

        [[nodiscard]] const std::vector<Camera*>& getCameras() const;

        void add(Camera* camera);

        void remove(Camera* toRemove);

        Camera* getActiveCamera();

        int enable(Camera* camera);

        int disable();

        bool isPresent(Camera* camera);

        int size();

    };
}
