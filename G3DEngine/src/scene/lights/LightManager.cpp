//
// Created by ubuntu on 30/11/20.
//

#include <algorithm>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include "LightManager.h"
#include <GL/freeglut.h>
#include "cglib.h"
#include <shading\ShaderProgramManager.h>

namespace CGLib {
    std::map<unsigned int, Light *> initializeActiveLightsMap() {

        std::map < unsigned int, Light * > map{};

        int maxLights;
        glGetIntegerv(GL_MAX_LIGHTS, &maxLights);

        for (auto i = 0; i < maxLights; i++)
            map[CGLIB_LIGHT0 + i] = nullptr;

        return map;

    }

    unsigned int getFirstAvailableSpot() {

        auto &lightsMap = LightManager::getInstance().m_active_lights;
        auto iterator = lightsMap.begin();

        while (iterator != lightsMap.end()) {
            if (iterator->second == nullptr)
                return iterator->first;
            ++iterator;
        }

        return 0;

    }

    void setLightRef(Light *light, unsigned int lightRef) {
        light->m_light_ref = lightRef;
    }

    LightManager::LightManager()
            : m_supported_lights(0),
              m_lights(std::vector < Light * > {}),
              m_active_lights(initializeActiveLightsMap()) {
        glGetIntegerv(GL_MAX_LIGHTS, &m_supported_lights);
    }

    LightManager& LightManager::getInstance()
    {
        static LightManager instance{};

        return instance;
    }

    int LightManager::getSupportedLights() const {
        return m_supported_lights;
    }

    Light *LightManager::getLight(const std::string &id) const {

        if (m_lights.empty())
            return nullptr;

        for (auto m_light : m_lights) {
            if (m_light->getId() == id) {
                return m_light;
            }
        }

        return nullptr;

    }

    bool LightManager::isPresent(Light *light) const {

        if (m_lights.empty())
            return false;

        for (auto m_light : m_lights) {
            if (m_light == light) {
                return true;
            }
        }

        return false;
    }

    Light *LightManager::getActiveLight(const std::string &id) const {

        if (m_lights.empty() && m_active_lights.empty())
            return nullptr;

        for (auto m_light : m_active_lights) {
            if (m_light.second != nullptr && m_light.second->getId() == id) {
                return m_light.second;
            }
        }

        return nullptr;

    }

    Light *LightManager::getActiveLight(unsigned int lightRef) const {

        return m_active_lights.at(lightRef);

    }

    unsigned int LightManager::enable(Light *light) {

        if (!isPresent(light))
            return 0;

        setLightRef(light, getFirstAvailableSpot());
        unsigned int ref = light->getLightRef();

        if (ref != 0) {
            m_active_lights.insert_or_assign(ref, light);
            light->enable();
        }

        return ref;

    }

    unsigned int LightManager::enable(const std::string &id) {

        Light *storedLight = getLight(id);

        if (storedLight == nullptr)
            return 0;

        setLightRef(storedLight, getFirstAvailableSpot());
        unsigned int ref = storedLight->getLightRef();

        if (ref != 0) {
            m_active_lights.insert_or_assign(ref, storedLight);
            storedLight->enable();
        }

        return ref;

    }

    unsigned int LightManager::disable(Light *light) {

        if (m_lights.empty() && m_active_lights.empty())
            return 0;

        Light *activeLight = getActiveLight(light->getLightRef());

        if (activeLight == nullptr)
            return 0;

        unsigned int ref = activeLight->getLightRef();
        setLightRef(activeLight, 0);

        if (ref != 0) {
            m_active_lights.insert_or_assign(ref, nullptr);
            activeLight->disable();
        }

        return ref;

    }

    unsigned int LightManager::disable(const std::string &id) {

        if (m_lights.empty() && m_active_lights.empty())
            return 0;

        Light *activeLight = getActiveLight(id);

        if (activeLight == nullptr)
            return 0;

        unsigned int ref = activeLight->getLightRef();
        setLightRef(activeLight, 0);

        if (ref != 0) {
            m_active_lights.insert_or_assign(ref, nullptr);
            activeLight->disable();
        }

        return ref;

    }

    void LightManager::add(Light *light) {
        m_lights.push_back(light);
    }

    void LightManager::remove(const std::string &id) {

        if (m_lights.empty())
            return;

        auto toRemove = std::find_if(m_lights.begin(),
                                     m_lights.end(),
                                     [&id](const Light *light) {
                                         return (light->getId() == id);
                                     });

        m_lights.erase(toRemove);

    }

    void LightManager::remove(Light *light) {

        if (m_lights.empty())
            return;

        auto toRemove = std::find_if(m_lights.begin(),
                                     m_lights.end(),
                                     [&light](const Light *next) {
                                         return (light == next);
                                     });

        m_lights.erase(toRemove);

    }

    void LightManager::setGlobalAmbient(const glm::vec4 &globalAmbient) {

        LightManager::m_global_ambient = globalAmbient;

        glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, 1.0f);


    }

	 glm::vec4 LightManager::getGlobalAmbient()
	 {
		  return m_global_ambient;
	 }


    int LightManager::size() {
        return m_lights.size();
    }

    int LightManager::enabled() {

        int enabled = 0;
        auto iterator = m_active_lights.begin();

        while (iterator != m_active_lights.end()) {
            if (iterator->second != nullptr)
                enabled++;
        }

        return enabled;

    }

    std::vector<Light *> LightManager::getAllLights() {
        return m_lights;
    }
}