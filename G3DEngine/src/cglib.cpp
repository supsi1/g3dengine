#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include <utility>
#include <string>

#include "scene/SceneGraph.h"
#include "camera/CameraManager.h"
#include "transform.h"
#include "logging/Log.h"
#include "cglib.h"
#include "scene/render/OvVR.h"
#include <FreeImage.h>

/**
* Wrapped FreeGLUT DisplayMode options
*/
unsigned int CGLIB_RGB = GLUT_RGB;
unsigned int CGLIB_RGBA = GLUT_RGBA;
unsigned int CGLIB_SINGLE_BUFFER = GLUT_SINGLE;
unsigned int CGLIB_DOUBLE_BUFFER = GLUT_DOUBLE;
unsigned int CGLIB_DEPTH_BUFFER = GLUT_DEPTH;

/**
* Wrapped OpenGL options
*/
const unsigned int CGLIB_CLEAR_COLOR = GL_COLOR_BUFFER_BIT;
const unsigned int CGLIB_CLEAR_DEPTH = GL_DEPTH_BUFFER_BIT;
const unsigned int CGLIB_DEPTH = GL_DEPTH_TEST;
const unsigned int CGLIB_LIGHTING = GL_LIGHTING;
const unsigned int CGLIB_MMODE_MODELVIEW = GL_MODELVIEW;
const unsigned int CGLIB_MMODE_PROJECTION = GL_PROJECTION;
const unsigned int CGLIB_CULLING = GL_CULL_FACE;
const unsigned int CGLIB_WIREFRAME = GL_LINE;
const unsigned int CGLIB_SOLID = GL_FILL;
const unsigned int CGLIB_BOTH = GL_FRONT_AND_BACK;
const unsigned int CGLIB_FRONT = GL_FRONT;
const unsigned int CGLIB_BACK = GL_BACK;
const unsigned int CGLIB_POSITION = GL_POSITION;
const unsigned int CGLIB_SPOT_CUTOFF = GL_SPOT_CUTOFF;
const unsigned int CGLIB_SPOT_DIRECTION = GL_SPOT_DIRECTION;
const unsigned int CGLIB_ATTENUATION_CONSTANT = GL_CONSTANT_ATTENUATION;
const unsigned int CGLIB_ATTENUATION_LINEAR = GL_LINEAR_ATTENUATION;
const unsigned int CGLIB_ATTENUATION_QUADRATIC = GL_QUADRATIC_ATTENUATION;
const unsigned int CGLIB_AMBIENT = GL_AMBIENT;
const unsigned int CGLIB_DIFFUSE = GL_DIFFUSE;
const unsigned int CGLIB_SPECULAR = GL_SPECULAR;
const unsigned int CGLIB_EMISSION = GL_EMISSION;
const unsigned int CGLIB_SHININESS = GL_SHININESS;
const unsigned int CGLIB_LIGHT0 = GL_LIGHT0;

CTX_STATUS* contextStatus = static_cast<CTX_STATUS*>(malloc(sizeof(CTX_STATUS)));;

void CGLib::init(const char* contextName, int* pargc, char** argv)
{
	contextStatus->contextName = contextName;
	contextStatus->nearPlane = 0.5f;
	contextStatus->farPlane = 10000.0f;
	contextStatus->fov = 45.0f;
	glutInitContextVersion(4, 4);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInit(pargc, argv);

	CGLib::enableContextFlags();
	FreeImage_Initialise();


	CG_CR_LOG_TRACE("FreeGLUT Context Initialized");
}

void CGLib::setWindowOptions(unsigned int displayMode, int windowW, int windowH, int windowX, int windowY)
{
	contextStatus->displayMode = displayMode;
	contextStatus->windowWidth = windowW;
	contextStatus->windowHeight = windowH;
	contextStatus->windowX = windowX;
	contextStatus->windowY = windowY;
	if ((displayMode & CGLIB_DEPTH_BUFFER) == CGLIB_DEPTH_BUFFER)
		contextStatus->isDepthBufferUsed = true;

	glutInitDisplayMode(contextStatus->displayMode);
	glutInitWindowSize(contextStatus->windowWidth, contextStatus->windowHeight);
	glutInitWindowPosition(contextStatus->windowX, contextStatus->windowY);
}

void CGLib::createWindow()
{
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	contextStatus->windowId = glutCreateWindow(contextStatus->contextName);
	CGLib::enableLights();
	CGLib::enableFaceCulling();
	CGLib::enableDepthTest();
	CGLib::enableSmoothShading();

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (err != GLEW_OK)
	{
		CG_CR_LOG_ERR("Error initializing Glew: {}", (int)err);
		return;
	}
	if (GLEW_VERSION_4_4)
		CG_CR_LOG_INFO("Driver supports OpenGL 4.4");
	else
	{
		CG_CR_LOG_ERR("OpenGL 4.4 not supported\n");
		return;
	}

	CGLib::registerDebugCallback();
	CGLib::enableDebugOutputSync();
	CGLib::enableTexture();


	CG_CR_LOG_INFO("Vendor: {}  ", glGetString(GL_VENDOR));
	CG_CR_LOG_INFO("Driver: {}  ", glGetString(GL_RENDERER));
	int oglVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &oglVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &oglVersion[1]);
	CG_CR_LOG_INFO("Version: {} [{}.{}] ", glGetString(GL_VERSION), oglVersion[0], oglVersion[1]);

	int oglContextProfile;
	glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &oglContextProfile);
	if (oglContextProfile & GL_CONTEXT_CORE_PROFILE_BIT)
		CG_CR_LOG_INFO("Core profile ");
	if (oglContextProfile & GL_CONTEXT_COMPATIBILITY_PROFILE_BIT)
		CG_CR_LOG_INFO("Compatibility profile ");

	int oglContextFlags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &oglContextFlags);
	if (oglContextFlags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT)
		CG_CR_LOG_INFO("Forward compatible ");
	if (oglContextFlags & GL_CONTEXT_FLAG_DEBUG_BIT)
		CG_CR_LOG_INFO("Debug flag ");
	if (oglContextFlags & GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT)
		CG_CR_LOG_INFO("Robust access flag ");
	if (oglContextFlags & GL_CONTEXT_FLAG_NO_ERROR_BIT)
		CG_CR_LOG_INFO("No error flag ");


	CG_CR_LOG_INFO("GLS: {} ", glGetString(GL_SHADING_LANGUAGE_VERSION));


	CG_CR_LOG_INFO("Glew Context Initialized");

}

void CGLib::setBackgroundColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void CGLib::enableContextFlags()
{
	glutInitContextFlags(GLUT_DEBUG);
}

void CGLib::enableTexture()
{
	 glEnable(GL_TEXTURE_2D);
}

void __stdcall DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
                             const GLchar* message, GLvoid* userParam)
{
	//TODO
	CG_CR_LOG_ERR("OpenGL debug message: {}", message);
	CG_CR_LOG_ERR("Source was: {}", source);
	CG_CR_LOG_ERR("Called with params: {}", userParam);
}

void CGLib::registerDebugCallback()
{
	glDebugMessageCallback((GLDEBUGPROC)DebugCallback, nullptr);
}

void CGLib::enableDebugOutputSync()
{
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
}

void CGLib::disableDebugOutputSync()
{
	glDisable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
}

void CGLib::clear()
{
	unsigned int toClear =
		CGLIB_CLEAR_COLOR | ((contextStatus->isDepthBufferUsed) ? CGLIB_CLEAR_DEPTH : 0x0000);

	glClear(toClear);
}

void CGLib::setDisplayCallback(void (*callback)())
{
	glutDisplayFunc(callback);
}

void CGLib::setWindowReshapeCallback(void (*callback)(int, int))
{
	glutReshapeFunc(callback);
}

void CGLib::setKeyboardEventCallback(void (*callback)(unsigned char key, int mouseX, int mouseY))
{
	glutKeyboardFunc(callback);
}

void CGLib::updateProjectionMatrix(int width, int height)
{
	contextStatus->windowWidth = width;
	contextStatus->windowHeight = height;
	glViewport(0, 0, width, height);
	contextStatus->projectionMatrix = getProjectionMatrix(contextStatus->fov, (float)width / (float)height, contextStatus->nearPlane, contextStatus->farPlane);
}

void CGLib::setProjectionMatrix(glm::mat4 projMatrix)
{
	 contextStatus->projectionMatrix = projMatrix;
}

void CGLib::disableLights()
{
	glDisable(CGLIB_LIGHTING);
	glDisable(GL_NORMALIZE);
}

void CGLib::enableLights()
{
	glEnable(CGLIB_LIGHTING);
	glEnable(GL_NORMALIZE);
}

void CGLib::enableSmoothShading()
{
	glShadeModel(GL_SMOOTH);
}

void CGLib::enableFlatShading()
{
	glShadeModel(GL_FLAT);
}

void CGLib::refreshDisplay()
{
	glutSwapBuffers();
	glutPostWindowRedisplay(contextStatus->windowId);
}

void CGLib::enterMainLoop()
{
	glutMainLoop();
}

void CGLib::enableDepthTest()
{
	glEnable(CGLIB_DEPTH);
}

void CGLib::disableDepthTest()
{
	glDisable(CGLIB_DEPTH);
}

void CGLib::enableFaceCulling()
{
	glEnable(CGLIB_CULLING);
}

void CGLib::disableFaceCulling()
{
	glDisable(CGLIB_CULLING);
}

void CGLib::setPolygonMode(unsigned int face, unsigned int mode)
{
	glPolygonMode(face, mode);
}

void CGLib::setAssetsDirPath(const char* dirPath)
{
	contextStatus->assetsDir = dirPath;
}

const char* CGLib::getAssetsDirPath()
{
	return contextStatus->assetsDir;
}

void CGLib::setMouseCallback(void (*callback)(int, int))
{
	glutPassiveMotionFunc(callback);
}

LIB_API glm::mat4 CGLib::getProjectionMatrix()
{
	return contextStatus->projectionMatrix;
}

CTX_STATUS* CGLib::getCtxStatus()
{
	return contextStatus;
}
