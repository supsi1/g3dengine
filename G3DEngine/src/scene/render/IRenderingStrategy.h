#pragma once
#include <Defs.h>
#include <vector>

namespace CGLib {
	class SceneNode;
	class Camera;

	class LIB_API IRenderingStrategy
	{
	public:

		virtual void init() = 0;
		virtual void deinit() = 0;
		virtual void render(std::vector<SceneNode*> nodes, Camera* camera) = 0;

	};
}
