#pragma once

#include "ShaderProgram.h"
#include <vector>
#include <Defs.h>
#include <glm/glm.hpp>

namespace CGLib {


	class LIB_API ShaderProgramManager
	{

	private:

		ShaderProgramManager();

		ShaderProgram m_defaultProgram;
		
		ShaderObject *m_defaultVertexShader;
		ShaderObject *m_defaultFragmentShader;
		
		std::vector<ShaderProgram *> m_programs;


	public:

		static ShaderProgramManager &getInstance();

		void addProgram(ShaderProgram* program);

		ShaderProgram* getDefaultProgram();
		void useProgram(ShaderProgram* program);

		void setDefaultFragmentShader(ShaderObject* fragmentShader);

		void setLight(int ref, glm::vec4 position, glm::vec4 diffuse, glm::vec4 specular, glm::vec4 ambient);
		void enableLight(int ref, bool enabled);

		void init();

		void deinit();

	};

}