#include "ShaderProgram.h"
#include <logging/Log.h>
#include <GL/glew.h>

CGLib::ShaderProgram::ShaderProgram()
{
	//Get default shader
}

void CGLib::ShaderProgram::setVertexShader(ShaderObject* shader)
{

	if (shader->getType() != CGLib::ShaderType::VERTEX) {
		CG_CR_LOG_ERR("Incorrect shader type. Default vertex shader was applied");
		return;
	}

	m_vertexShader = shader;

}

void CGLib::ShaderProgram::setFragmentShader(ShaderObject* shader)
{

	if (shader->getType() != CGLib::ShaderType::FRAGMENT) {
		CG_CR_LOG_ERR("Incorrect shader type. Default fragment shader was applied");
		return;
	}

	m_fragmentShader = shader;

}

void CGLib::ShaderProgram::setGeometryShader(ShaderObject* shader)
{

	if (shader->getType() != CGLib::ShaderType::GEOMETRY) {
		CG_CR_LOG_ERR("Incorrect shader type.");
		return;
	}

	m_geometryShader = shader;

}

void CGLib::ShaderProgram::setComputeShader(ShaderObject* shader)
{

	if (shader->getType() != CGLib::ShaderType::COMPUTE) {
		CG_CR_LOG_ERR("Incorrect shader type.");
		return;
	}

	m_computeShader = shader;

}

bool CGLib::ShaderProgram::isValid() const {
	if (m_fragmentShader == nullptr || m_vertexShader == nullptr) return false;
	if (m_id == 0) return false;
	return true;
}

unsigned int CGLib::ShaderProgram::getId() const
{
	return m_id;
}

void CGLib::ShaderProgram::createProgram()
{
	
	m_id = glCreateProgram();

	// CREATE SHADERS

	m_vertexShader->createShader();
	m_vertexShader->compileShader();
	glAttachShader(m_id, m_vertexShader->m_id);

	if (m_fragmentShader != nullptr) {
		m_fragmentShader->createShader();
		m_fragmentShader->compileShader();
		glAttachShader(m_id, m_fragmentShader->m_id);
	}	

	if (m_geometryShader != nullptr) {
		m_geometryShader->createShader();
		m_geometryShader->compileShader();
		glAttachShader(m_id, m_geometryShader->m_id);
	}	
	
	if (m_computeShader != nullptr) {
		m_computeShader->createShader();
		m_computeShader->compileShader();
		glAttachShader(m_id, m_computeShader->m_id);
	}

	glLinkProgram(m_id);
}

void CGLib::ShaderProgram::deleteProgram()
{

	m_vertexShader->deleteShader();

	if (m_fragmentShader != nullptr)
		m_fragmentShader->deleteShader();

	if (m_geometryShader != nullptr)
		m_geometryShader->deleteShader();

	if (m_computeShader != nullptr)
		m_computeShader->deleteShader();

	glDeleteProgram(m_id);

}
