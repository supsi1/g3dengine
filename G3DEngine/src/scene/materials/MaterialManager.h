#pragma once

#include <string>
#include <unordered_map>
#include "Material.h"

#include "Defs.h"

namespace CGLib {
    class LIB_API MaterialManager {
    private:
        std::unordered_map<std::string, Material *> materialMap{};

        MaterialManager() = default;

    public:
        static MaterialManager &getInstance();

        void addMaterial(std::string name, Material *material);

        void removeMaterial(std::string name);

        Material *getMaterial(std::string name);

    };
}
