//
// Created by kalu on 12/25/20.
//

#include <GL/glew.h>
#include "BilinearFilteringStrategy.h"
#include <GL/freeglut.h>
namespace CGLib {
    void BilinearFilteringStrategy::execute() {
        LinearFilteringStrategy::execute();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    }
}
