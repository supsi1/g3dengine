#pragma once

#include "Defs.h"

#include <utility>

#include "materials/Material.h"
#include "SceneNode.h"
#include "materials/Texture2D.h"
namespace CGLib {

    class LIB_API Mesh : public SceneNode {

    protected:

        Material *m_material;
        int m_verticesNumber;
        int m_trianglesNumber;

        const glm::vec3 *m_vertices;
        const unsigned int *m_triangles;
        const glm::vec2 *m_mapping = nullptr;
        const glm::vec3 *m_normals = nullptr;

        unsigned int m_verticesVbo = 0;
        unsigned int m_mappingVbo = 0;
        unsigned int m_normalsVbo = 0;
        unsigned int m_facesVbo = 0;
        unsigned int m_vao = 0;

    	bool m_skipCulling = false;

        glm::vec3 bboxMin;
        glm::vec3 bboxMax;

        void renderMesh() const;

    public:

        Mesh(const glm::vec3 *vertices,
             const unsigned int *triangles,
             int vertices_number,
             int triangles_number,
             Material *material = new Material{},
             const glm::vec3 &rotate = glm::vec3{0.0f},
             const glm::vec3 &translate = glm::vec3{0.0f},
             const glm::vec3 &scale = glm::vec3{1.0f})
                : SceneNode(rotate, translate, scale),
                  m_vertices(vertices),
                  m_triangles(triangles),
                  m_material(material),
                  m_trianglesNumber(triangles_number),
                  m_verticesNumber(vertices_number) {
            setId(getId().append("mesh"));
        }

        ~Mesh();

        void setNormals(glm::vec3 *normals);

        [[nodiscard]] const unsigned int *getTriangles() const;

        [[nodiscard]] const glm::vec3 *getVertices() const;

        [[nodiscard]] const glm::vec3 *getVertexInTriangle(int triangleIndex, int vertexIndex) const;

        [[nodiscard]] Material *getMaterial();

        void setMaterial(Material *material);

        void render(glm::mat4 camera, glm::mat4 objectMatrix) override;

        [[nodiscard]] const glm::vec2 *getMapping() const;

        void setMapping(const glm::vec2 *mMapping);

        [[nodiscard]] int getVerticesNumber() const;

        [[nodiscard]] int getFacesNumber() const;

        void setBboxMin(glm::vec3 min);

        void setBboxMax(glm::vec3 max);

        void setVertices_number(int m_vertices_number)
        {
	        m_verticesNumber = m_vertices_number;
        }

        void setVertices(const glm::vec3* m_vertices)
        {
	        this->m_vertices = m_vertices;
        }


        std::string getType() override;
    	
    	glm::vec3 getBBoxMin();
    	glm::vec3 getBBoxMax();

        void setSkipCulling(bool skip);
        bool isSkipCulling();
    	
        virtual void init() override;
        virtual void deinit() override;
    };
}
