//
// Created by kalu on 12/24/20.
//

#include <CGEngine.h>
#include "test_MaterialManager.h"

using namespace CGLib;

void suite_materialmanager() {
    auto* m = new CGLib::Material();
    CGLib::MaterialManager::getInstance().addMaterial("testMat", m);
    auto *found = MaterialManager::getInstance().getMaterial("testMat");

    assert(m == found);

    delete m;
}