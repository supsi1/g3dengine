#include "Frustum.h"
#include "logging/Log.h"


CGLib::Frustum::Frustum(glm::mat4 projection, glm::mat4 modelView) : m_projection(projection), m_modelView(modelView)
{
}

bool CGLib::Frustum::isInside(glm::vec3 point) const
{
	const glm::vec4 newPoint = m_projection * m_modelView *  glm::vec4(point,1.0f);

	const bool inLeft = -newPoint.w < newPoint.x;
	const bool inRight = newPoint.x < newPoint.w;
	const bool inBot = -newPoint.w < newPoint.y;
	const bool inTop = newPoint.y < newPoint.w;
	const bool inNear = -newPoint.w < newPoint.z;
	const bool inFar = newPoint.z < newPoint.w;
	
  return (inLeft && inRight && inBot && inTop && inNear && inFar);

}

glm::mat4 CGLib::Frustum::getProjection() const
{
	return m_projection;
}

glm::mat4 CGLib::Frustum::getModelview() const
{
	return m_modelView;
}

void CGLib::Frustum::setProjection(glm::mat4& proj)
{
	m_projection = proj;
}

void CGLib::Frustum::setModelView(glm::mat4& modelView)
{
	m_modelView = modelView;
}

void CGLib::Frustum::setEnabled(bool enabled)
{
	this->enabled = enabled;
}

bool CGLib::Frustum::isEnabled() const
{
	return enabled;
}
