#pragma once

#include "Defs.h"

#include "SceneNode.h"
#include "../camera/Camera.h"

namespace CGLib {
	class IRenderingStrategy;

	class LIB_API SceneGraph {

    public:
        static SceneGraph &getInstance();

        [[nodiscard]] std::vector<SceneNode *> getRootNodes() const;

        void setRootNodes(std::vector<SceneNode *> &newRootNodes);

        void addRootNode(SceneNode *rootNode);

        void renderScene(Camera *camera) const;

        [[nodiscard]] SceneNode *findNode(std::string nodeId) const;

        [[nodiscard]] SceneNode *popNode(const std::string &nodeId) const;

        void setRenderingStrategy(IRenderingStrategy* strategy);

        void init();

        void deinit();

    private:

        SceneGraph() = default;

        IRenderingStrategy *m_strategy;

        std::vector<SceneNode *> m_rootNodes;
    };
}

