#include "GUIManager.h"
#include "GL/glew.h"
#include "../vendor/imgui/imgui.h"
#include "../vendor/imgui/imgui_impl_glut.h"
#include "../vendor/imgui/imgui_impl_opengl3.h"
#include "logging/Log.h"
#include "cglib.h"


namespace CGLib
{
	GUIManager& GUIManager::getInstance()
	{
		static GUIManager instance{};
		return instance;
	}

	void GUIManager::init()
	{
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO &io = ImGui::GetIO();
		io.DisplaySize.x = getCtxStatus()->windowWidth;
		io.DisplaySize.y = getCtxStatus()->windowHeight;

		ImGui::StyleColorsDark();
		ImGui_ImplGLUT_Init();
		ImGui_ImplGLUT_InstallFuncs();
		ImGui_ImplOpenGL3_Init();

		CG_CR_LOG_INFO("Initialized GUI Manager");
	}

	void GUIManager::drawGUI() const
	{
		glUseProgram(0);

		ImGuiIO &io = ImGui::GetIO();
		io.DisplaySize.x = getCtxStatus()->windowWidth;
		io.DisplaySize.y = getCtxStatus()->windowHeight;

		
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGLUT_NewFrame();
		
		ImGui::SetNextWindowPos(ImVec2{10,20});
		ImGui::SetNextWindowCollapsed(false);
		
		ImGui::Begin("Statistics", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
		{
			ImGui::SetWindowFontScale(m_scale);
			ImGui::Text("Average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate,
                        ImGui::GetIO().Framerate);
			ImGui::NewLine();
			ImGui::Text("Last frame rendered meshes: %d", getCtxStatus()->renderedMeshes);
		}
		ImGui::End();
		
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}

	void GUIManager::deinit()
	{
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGLUT_Shutdown();
		ImGui::DestroyContext();
	}

	void GUIManager::setUIScale(float factor)
	{
		m_scale = factor;
	}
}
