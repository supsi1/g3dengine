#pragma once

#include "Defs.h"
#include <spdlog/spdlog.h>

namespace CGLib {

	class LIB_API Log
	{
	public:
		static Log* getInstance();

		inline std::shared_ptr<spdlog::logger> getCoreLogger() {
			return s_coreLogger;
		};

		inline std::shared_ptr<spdlog::logger> getClientLogger() {
			return s_clientLogger;
		};

		void setClientLevel(spdlog::level::level_enum level);
		void setCoreLevel(spdlog::level::level_enum level);

	private:
		Log();
		std::shared_ptr<spdlog::logger> s_coreLogger;
		std::shared_ptr<spdlog::logger> s_clientLogger;
	};

}

#define CG_LOG_TRACE(...) CGLib::Log::getInstance()->getClientLogger()->trace(__VA_ARGS__)
#define CG_LOG_INFO(...) CGLib::Log::getInstance()->getClientLogger()->info(__VA_ARGS__)
#define CG_LOG_WARN(...) CGLib::Log::getInstance()->getClientLogger()->warn(__VA_ARGS__)
#define CG_LOG_ERR(...) CGLib::Log::getInstance()->getClientLogger()->error(__VA_ARGS__)

// Only for core build in debug
#ifdef CGLIB_EXPORTS
	#ifndef CGLIB_RELEASE

		#define CG_CR_LOG_TRACE(...) CGLib::Log::getInstance()->getCoreLogger()->trace(__VA_ARGS__)
		#define CG_CR_LOG_INFO(...) CGLib::Log::getInstance()->getCoreLogger()->info(__VA_ARGS__)
		#define CG_CR_LOG_WARN(...) CGLib::Log::getInstance()->getCoreLogger()->warn(__VA_ARGS__)
		#define CG_CR_LOG_ERR(...) CGLib::Log::getInstance()->getCoreLogger()->error(__VA_ARGS__)

	#else

		#define CG_CR_LOG_TRACE(...) void()
		#define CG_CR_LOG_INFO(...) void()
		#define CG_CR_LOG_WARN(...) void()
		#define CG_CR_LOG_ERR(...) void()


	#endif
#endif