//
// Created by ubuntu on 23/12/20.
//

#include <CGEngine.h>
#include "test_LightManager.h"

using namespace CGLib;

void test_add_remove_size(){

    CGLib::LightManager instance = CGLib::LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    assert(instance.size() == 0);

    instance.add(l1);

    assert(instance.size() == 1);

    instance.remove(l1);

    assert(instance.size() == 0);


}

void test_ispresent(){

    CGLib::LightManager instance = CGLib::LightManager::getInstance();

    CGLib::Light *l1 = CGLib::LightBuilder::getInstance()
            .buildDirectional();

    assert(instance.isPresent(l1) == false);

    instance.add(l1);

    assert(instance.isPresent(l1) == true);

    instance.remove(l1);

    assert(instance.isPresent(l1) == false);

}

void test_getlight(){

    CGLib::LightManager instance = CGLib::LightManager::getInstance();

    CGLib::Light *l1 = CGLib::LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);

    assert(instance.getLight(l1->getId()) == l1);

    instance.remove(l1);

    assert(instance.getLight(l1->getId()) == nullptr);

}

void test_enable_disable(){

    CGLib::LightManager instance = CGLib::LightManager::getInstance();

    CGLib::Light *l1 = CGLib::LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);
    instance.enable(l1);

    assert(l1->getLightRef() != 0);

    instance.disable(l1);

    assert(l1->getLightRef() == 0);

    instance.remove(l1);

}

void test_getactivelight(){

    CGLib::LightManager instance = CGLib::LightManager::getInstance();

    CGLib::Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);
    instance.enable(l1);

    assert(instance.getActiveLight(l1->getId()) == l1);

    instance.disable(l1);

    assert(instance.getActiveLight(l1->getId()) == nullptr);

    instance.remove(l1);

}

void suite_lightmanager(){

    test_add_remove_size();
    test_ispresent();
    test_getlight();
    test_enable_disable();
    test_getactivelight();

}