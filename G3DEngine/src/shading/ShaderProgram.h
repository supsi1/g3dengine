#pragma once

#include "ShaderObject.h"
#include <Defs.h>

namespace CGLib {


	class LIB_API ShaderProgram
	{

	private:

		unsigned int m_id = 0;

		ShaderObject *m_vertexShader = nullptr;
		ShaderObject *m_fragmentShader = nullptr;
		ShaderObject *m_geometryShader = nullptr;
		ShaderObject *m_computeShader = nullptr;

		void createProgram();
		void deleteProgram();

	public:

		ShaderProgram();

		void setVertexShader(ShaderObject* shader);

		void setFragmentShader(ShaderObject* shader);

		void setGeometryShader(ShaderObject* shader);

		void setComputeShader(ShaderObject* shader);

		bool isValid() const;

		unsigned int getId() const;

		friend class ShaderProgramManager;
		friend class Skybox;

	};

}