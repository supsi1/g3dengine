#pragma once

#include <Defs.h>
#include <cstdio>

namespace CGLib {

	enum LIB_API ShaderType
	{
		VERTEX = 0,
		FRAGMENT,
		GEOMETRY,
		COMPUTE
	};

	class LIB_API ShaderObject
	{

	private:

		char* m_source;
		unsigned int m_id;
		ShaderType	m_type;

		void createShader();
		void compileShader() const;
		void deleteShader();

	public:

		ShaderObject(char* source, ShaderType type);
		ShaderObject(FILE* shaderFile, ShaderType type);

		unsigned int getId() const;

		ShaderType getType() const;

		void displayShaderInfo() const;

		friend class ShaderProgram;

	};

}