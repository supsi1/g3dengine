#include "SceneObject.h"
#include "LuminousObject.h"

namespace CGLib {
    LuminousObject::LuminousObject(
            const glm::vec4 &diffuse,
            const glm::vec4 &ambient,
            const glm::vec4 &specular,
            const glm::vec4 &emission)
            : m_diffuse(diffuse), m_ambient(ambient), m_specular(specular), m_emission(emission) {}

    const glm::vec4 &LuminousObject::getDiffuse() const {
        return m_diffuse;
    }

    void LuminousObject::setDiffuse(const glm::vec4 &diffuse) {
        LuminousObject::m_diffuse = diffuse;
    }

    void LuminousObject::setDiffuse(const glm::vec3 &diffuse) {
        glm::vec4 diff{diffuse.r, diffuse.g, diffuse.b, 1.0f};
        this->m_diffuse = diff;
    }

    const glm::vec4 &LuminousObject::getAmbient() const {
        return m_ambient;
    }

    void LuminousObject::setAmbient(const glm::vec4 &ambient) {
        LuminousObject::m_ambient = ambient;
    }

    void LuminousObject::setAmbient(const glm::vec3 &ambient) {
        glm::vec4 amb{ambient.r, ambient.g, ambient.b, 1.0f};
        this->m_ambient = amb;
    }

    const glm::vec4 &LuminousObject::getEmission() const {
        return m_emission;
    }

    void LuminousObject::setEmission(const glm::vec4 &emission) {
        m_emission = emission;
    }

    void LuminousObject::setEmission(const glm::vec3 &emission) {
        glm::vec4 em{emission.r, emission.g, emission.b, 1.0f};
    }

    const glm::vec4 &LuminousObject::getSpecular() const {
        return m_specular;
    }

    void LuminousObject::setSpecular(const glm::vec4 &specular) {
        LuminousObject::m_specular = specular;
    }

    void LuminousObject::setSpecular(const glm::vec3 &specular) {
        glm::vec4 spec{specular.r, specular.g, specular.b, 1.0f};
        this->m_specular = spec;
    }


}