#pragma once

#include "../scene/SceneNode.h"
#include "../parser/ParsingStrategy.h"
#include "Defs.h"

namespace CGLib {
    class LIB_API FileParser {
    private:
        ParsingStrategy *m_strategy;

    public:
        explicit FileParser(ParsingStrategy *strategy = nullptr);

        ~FileParser();

        void setStrategy(ParsingStrategy *strategy);

        SceneNode *loadFromFile(const std::string &fileName, bool assetsDirRelative = true);

    };
}