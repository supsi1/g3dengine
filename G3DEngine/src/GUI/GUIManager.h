#pragma once
struct ImGuiIO;

#include "Defs.h"

namespace CGLib
{
	class LIB_API GUIManager
{
public:
	static GUIManager& getInstance();

	static void init();
	static void deinit();
	void setUIScale(float factor);
	void drawGUI() const;

private:
	float m_scale = 1;
	GUIManager() = default;
};
}

