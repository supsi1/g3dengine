#pragma once
#include "Texture.h"
#include "Defs.h"
#include <string>

namespace CGLib {
	 class LIB_API Texture3D : public Texture
	 {
		  unsigned char* m_bitmap[6];
		  std::string images[6];

	 public:
		  void setBitmap(unsigned char* bitmap, unsigned int index);
		  unsigned char* getBitmap(unsigned int index);
		  void load();
		  void bind();
		  void setImage(std::string path, unsigned int index);
	 };
}

