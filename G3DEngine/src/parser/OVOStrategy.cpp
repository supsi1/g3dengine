#include "OVOStrategy.h"
#include <iostream>
#include <vector>
#include <memory.h>
#include "scene/SceneGraph.h"
#include "scene/lights/DirectionalLight.h"
#include "scene/lights/OmniDirectionalLight.h"
#include "scene/lights/LightBuilder.h"
#include <glm/gtc/packing.hpp>
#include <scene/Mesh.h>
#include <scene/lights/LightManager.h>
#include <scene/materials/MaterialManager.h>
#include <FreeImage.h>
#include <scene/EmptyNode.h>
#include <scene/materials/TextureManager.h>

#define uint unsigned int

namespace CGLib {
// Stripped-down redefinition of OvObject (just for the chunk IDs):
    class OvObject {
    public:
        enum class Type : int  ///< Type of entities
        {
            // Foundation types:
            VERSION = 0,
            NODE,
            OBJECT2D,
            OBJECT3D,
            LIST,

            // Derived classes:
            BUFFER,
            SHADER,
            TEXTURE,
            FILTER,
            MATERIAL,
            FBO,
            QUAD,
            BOX,
            SKYBOX,
            FONT,
            CAMERA,
            LIGHT,
            BONE,
            MESH,       // Keep them...
            SKINNED, // ...consecutive
            INSTANCED,
            PIPELINE,
            EMITTER,

            // Animation type
            ANIM,

            // Physics related:
            PHYSICS,

            // Terminator:
            LAST,
        };
    };

// Stripped-down redefinition of OvMesh (just for the subtypes):
    class OvMesh {
    public:
        enum class Subtype : int ///< Kind of mesh
        {
            // Foundation types:
            DEFAULT = 0,
            NORMALMAPPED,
            TESSELLATED,

            // Terminator:
            LAST,
        };
    };

// Stripped-down redefinition of OvLight (just for the subtypes):
    class OvLight {
    public:
        enum class Subtype : int ///< Kind of light
        {
            // Foundation types:
            OMNI = 0,
            DIRECTIONAL,
            SPOT,

            // Terminator:
            LAST,
        };
    };

    glm::vec3 parseVec3(char *buffer, uint &offset) {
        glm::vec3 vec{};
        memcpy(&vec, buffer + offset, sizeof(glm::vec3));
        offset += sizeof(glm::vec3);
        return vec;
    }

    std::string parseString(char *buffer, uint &offset) {
        uint charOffset = 0;
        while (*(buffer + offset + charOffset) != 0) {
            ++charOffset;
        }

        ++charOffset;
        char *str = (char *) malloc(charOffset);
        memcpy(str, buffer + offset, charOffset);
        offset += charOffset;

        auto toRet = std::string{str};
        free(str);
        return toRet;
    }

    glm::mat4 parseMatrix(char *buffer, uint &offset) {
        glm::mat4 matrix{};
        memcpy(&matrix, buffer + offset, sizeof(matrix));
        offset += sizeof(glm::mat4);
        return matrix;
    }

    uint parseInteger(char *buffer, uint &offset) {
        uint integer;
        memcpy(&integer, buffer + offset, sizeof(uint));
        offset += sizeof(uint);
        return integer;
    }

    float parseFloat(char *buffer, uint &offset, bool half = false) {
        //TODO half float
        float floating;
        memcpy(&floating, buffer + offset, sizeof(uint));
        offset += sizeof(uint);
        return floating;
    }

    void skipPhysics(char *buffer, uint &offset) {
        struct PhysProps {
            unsigned char type;
            unsigned char contCollisionDetection;
            unsigned char collideWithRBodies;
            unsigned char hullType;

            // Vector data:
            glm::vec3 massCenter;

            // Mesh properties:
            float mass;
            float staticFriction;
            float dynamicFriction;
            float bounciness;
            float linearDamping;
            float angularDamping;
            unsigned int nrOfHulls;
            void *physObj;
            void *hull;
        };

        PhysProps mp;
        memcpy(&mp, buffer + offset, sizeof(PhysProps));
        offset += sizeof(PhysProps);

        for (int i = 0; i < mp.nrOfHulls; i++) {
            uint hullVertexNum = parseInteger(buffer, offset);
            uint hullFacesNum = parseInteger(buffer, offset);
            offset = offset + 12 + (hullVertexNum * sizeof(glm::vec3)) + (hullFacesNum * 3 * sizeof(uint));
        }
    }

    uint parseByte(char *buffer, uint &offset) {
        uint read = 0;
        memcpy(&read, buffer + offset, 1);
        ++offset;
        return read;
    }

    Light *parseLightData(char *buffer, uint &offset) {
        /*
         * Light:
         *  + light subtype (1 byte : OvLight::SUBTYPE)
         *  + rgb color (12 bytes : Rx4, Gx4, Bx4)
         *  - influence radius (4 bytes : float)
         *  + light direction (12 bytes : X:4, Y:4, Z:4)
         *  + cutoff (4 bytes: float)
         *  - spot exponent (4 bytes: float)
         *  - shadows? (1 byte: bool)
         *  - volumetric? (1 byte: bool)
         */
        Light *light = nullptr;

        auto lightType = (OvLight::Subtype) parseByte(buffer, offset);
        glm::vec3 color = parseVec3(buffer, offset);
        float radius = parseFloat(buffer, offset);
        glm::vec3 direction = parseVec3(buffer, offset);
        float cutoff = parseFloat(buffer, offset);

        //skip spot exponent
        offset += sizeof(float);

        //skip flags
        offset += 2;

        switch (lightType) {
            case OvLight::Subtype::DIRECTIONAL:
                light = LightBuilder::getInstance().buildDirectional();
                ((DirectionalLight*)light)->setDirection(direction);
                break;
            case OvLight::Subtype::OMNI:
                light = LightBuilder::getInstance().buildOmniDirectional();
                break;
            case OvLight::Subtype::SPOT:
                light = LightBuilder::getInstance().buildSpot();
                ((SpotLight *) light)->setCutoff(cutoff);
                ((SpotLight *) light)->setDirection(direction);
                break;
        }

        if (light != nullptr) {
            // set light props
            light->setAmbient(color);
            light->setDiffuse(color);
            light->setSpecular(color);

            light->setInfluence(radius);
            LightManager::getInstance().add(light);
            LightManager::getInstance().enable(light);
        }

        return light;
    }


    Mesh *parseMeshData(char *buffer, uint &offset) {
        Mesh *parsed;
        //discard mesh subtype
        offset += 1;

        //material name
        std::string matName = parseString(buffer, offset);

        // discard mesh radius, min bound box, max bound box
        offset += sizeof(float);
        //2 * sizeof(glm::vec3);

        glm::vec3 bboxMin = parseVec3(buffer, offset);
        glm::vec3 bboxMax = parseVec3(buffer, offset);

        if (parseByte(buffer, offset) != 0) {
            skipPhysics(buffer, offset);
        }

        // start parsing LODs
        // list of pair --> each pair is a list of vertices and a list of triangles
        std::vector <std::pair<glm::vec3 *, uint *>> LODsList;
        std::vector < glm::vec2 * > LODsTextureData;
        std::vector < glm::vec3 * > LODsNormalData;

        std::vector<int> LODsVerticesNumber;
        std::vector<int> LODsFacesNumber;

        uint lodNumber = parseInteger(buffer, offset);
        LODsList.reserve(lodNumber);
        LODsTextureData.reserve(lodNumber);
        LODsNormalData.reserve(lodNumber);

        for (uint i = 0; i < lodNumber; i++) {
            uint nVertices = parseInteger(buffer, offset);
            uint nFaces = parseInteger(buffer, offset);

            auto vertices = (glm::vec3 *) malloc(sizeof(glm::vec3) * nVertices);
            auto normals = (glm::vec3 *) malloc(sizeof(glm::vec3) * nVertices);
            auto textCoords = (glm::vec2 *) malloc(sizeof(glm::vec2) * nVertices);
            auto triangles = (uint *) malloc(sizeof(glm::vec3) * nFaces);

            //parse vertices
            for (uint j = 0; j < nVertices; j++) {
                glm::vec3 vertex;
                glm::vec4 normal;
                vertex = parseVec3(buffer, offset);
                vertices[j] = vertex;
                // skip normal
                normal = glm::unpackSnorm3x10_1x2(parseInteger(buffer, offset));
                normals[j] = glm::vec3{normal.x, normal.y, normal.z};

                glm::vec2 textureCoords = glm::unpackHalf2x16(parseInteger(buffer, offset));
                textCoords[j] = textureCoords;
                //skip tangent vector
                offset += sizeof(uint);
            }

            for (uint j = 0; j < nFaces * 3; j++) {
                uint face = parseInteger(buffer, offset);
                triangles[j] = face;
            }

            auto pair = std::pair < glm::vec3 *,
            uint*>{ vertices, triangles };
            LODsList.push_back(pair);
            LODsTextureData.push_back(textCoords);
            LODsNormalData.push_back(normals);
            LODsFacesNumber.push_back(nFaces);
            LODsVerticesNumber.push_back(nVertices);

        }

        // add LODs to mesh TODO
        parsed = new Mesh{LODsList.at(0).first, LODsList.at(0).second, LODsVerticesNumber.at(0), LODsFacesNumber.at(0)};
        parsed->setMapping(LODsTextureData.at(0));
        parsed->setNormals(LODsNormalData.at(0));
        parsed->setMaterial(MaterialManager::getInstance().getMaterial(matName));
        parsed->setBboxMax(bboxMax);
        parsed->setBboxMin(bboxMin);
        return parsed;
    }

    OvObject::Type getNextChunk(FILE *ovoFile, char **out_destBuffer) {
        //read chunk type and size
        OvObject::Type chunkType;
        uint chunkSize;
        fread(&chunkType, sizeof(uint), 1, ovoFile);
        fread(&chunkSize, sizeof(uint), 1, ovoFile);

        //load the whole chunk to memory
        (*out_destBuffer) = (char *) malloc(chunkSize);
        fread(*out_destBuffer, chunkSize, 1, ovoFile);

    	if(feof(ovoFile)) return OvObject::Type::LAST;

        return chunkType;
    }

    SceneNode *parseNodeFromChunk(FILE *file, char *chunkDataBuffer, OvObject::Type chunkType) {
        SceneNode *node = nullptr;

        // Parse nodes common information
        uint offset = 0;
        std::string name = parseString(chunkDataBuffer, offset);
        glm::mat4 transform = parseMatrix(chunkDataBuffer, offset);
        uint numChildren = parseInteger(chunkDataBuffer, offset);
        std::string targetName = parseString(chunkDataBuffer, offset);

        // Parse node specific information
        switch (chunkType) {
            case OvObject::Type::MESH:
                //TODO
                node = parseMeshData(chunkDataBuffer, offset);
                break;
            case OvObject::Type::LIGHT:
                node = parseLightData(chunkDataBuffer, offset);
                break;
            case OvObject::Type::NODE:
                node = new EmptyNode();
                break;
        }

        if (node != nullptr) {
            // set node transforms
            node->setBaseTransform(transform);
            node->setId(name);
            // Parse child nodes
            for (int i = 0; i < numChildren; i++) {
                OvObject::Type nextChunkType = getNextChunk(file, &chunkDataBuffer);
                node->addChild(parseNodeFromChunk(file, chunkDataBuffer, nextChunkType));
            }
        }

        if(name=="root")
        {
	        int a;
        }
    	
        return node;
    }

    Texture2D *parseTexture(std::string fileName) {
        if (fileName == "[none]") {
            return nullptr;
        }

        std::string assetsDir{CGLib::getAssetsDirPath()};
        fileName = assetsDir + separator() + fileName;

        

        FIBITMAP *bitmap = FreeImage_Load(
                FreeImage_GetFileType(fileName.c_str(), 0),
                fileName.c_str());
        FreeImage_FlipVertical(bitmap);

        auto texture = new Texture2D();
        texture->setBitmap(FreeImage_GetBits(bitmap));
        texture->setHeight(FreeImage_GetHeight(bitmap));
        texture->setWidth(FreeImage_GetWidth(bitmap));

        texture->loadToGPU();

        FreeImage_Unload(bitmap);

        return texture;
    }

    void parseMaterialToManager(char *buffer) {
        uint offset = 0;

        std::string matName = parseString(buffer, offset);
        glm::vec3 emission = parseVec3(buffer, offset);
        glm::vec3 albedo = parseVec3(buffer, offset);
        float roughness = parseFloat(buffer, offset);
        // 2 float follows
        offset += 8;

        // read albedo name
        std::string texName = parseString(buffer, offset);
        auto texture = parseTexture(texName);

        if (texture)
            TextureManager::getInstance().addTexture(texture->getTexId(), texture);

        // 4 bytes follows, we can void skip them because we are at the end of the chunk

        //convert to phong shading
        glm::vec3 ambient = albedo * 0.2f;
        glm::vec3 specular = albedo * 0.5f;
        glm::vec3 diffuse = albedo * 0.8f;
        float shine = (1.0f - (float) sqrt(roughness)) * 128.0f;

        auto *final = new Material();
        final->setShine(shine);
        final->setAmbient(ambient);
        final->setDiffuse(diffuse);
        final->setSpecular(specular);
        final->setEmission(emission);
        final->setTexture(texture);

        MaterialManager::getInstance().addMaterial(matName, final);
    }

    SceneNode *OVOStrategy::parse(FILE *fp) {
        SceneNode *root = new EmptyNode();

        while (!feof(fp)) {
            char *buffer;
            OvObject::Type chunkType = getNextChunk(fp, &buffer);
        	

            /*
             * discretize by chunk type:
             *  - node, mesh, light =  are all nodes and will be parsed with parseNodeFromChunk
             *  - material = an array of material is created and filled. at the end of the parsing,
             *               nodes that have a material will be injected with the respective one.
            */

            SceneNode *parsed;

            switch (chunkType) {
                // Object read is a NODE, so it must search for it's children too
                case OvObject::Type::NODE:
                case OvObject::Type::MESH:
                case OvObject::Type::LIGHT:
                    parsed = parseNodeFromChunk(fp, buffer, chunkType);
                    root->addChild(parsed);
                    break;
                case OvObject::Type::MATERIAL:
                    parseMaterialToManager(buffer);
                    break;
                default:
                    break;
            }
        }

        return root;
    }

    void OVOStrategy::execute() {
        this->m_parsed = parse(this->m_fp);
    }
}