
#include "render/IndirectRenderingList.h"
#include "SceneGraph.h"
#include "camera/Camera.h"
#include "logging/Log.h"
#include "render/IRenderingStrategy.h"

namespace CGLib {
    SceneGraph &SceneGraph::getInstance() {
        static SceneGraph instance{};

        return instance;
    }

    void SceneGraph::renderScene(Camera *camera) const {
        m_strategy->render(m_rootNodes, camera);
    }

    void SceneGraph::addRootNode(SceneNode *rootNode) {
        m_rootNodes.push_back(rootNode);
    }

    void SceneGraph::setRootNodes(std::vector<SceneNode *> &newRootNodes) {
        this->m_rootNodes = newRootNodes;
    }

    std::vector<SceneNode *> SceneGraph::getRootNodes() const {
        return this->m_rootNodes;
    }

    SceneNode *SceneGraph::findNode(std::string nodeId) const {
        for (auto node : m_rootNodes) {
            SceneNode *found = node->findNodeInChildren(nodeId);
            if (found) return found;
        }

        return nullptr;
    }

    SceneNode *SceneGraph::popNode(const std::string &nodeId) const {
        SceneNode *node = findNode(nodeId);
        node->getParent()->removeChildren(nodeId);
        return node;
    }

    void SceneGraph::setRenderingStrategy(IRenderingStrategy* strategy)
    {
        m_strategy = strategy;
    }
    
    void SceneGraph::init()
    {
        CG_CR_LOG_INFO("Initializing scene");
        for (auto& node : m_rootNodes) {
            node->initAll();
        }
        m_strategy->init();
        CG_CR_LOG_INFO("Initialized scene");
    }

    void SceneGraph::deinit()
    {
        CG_CR_LOG_INFO("Freeing scene");
        for (auto& node : m_rootNodes) {
            node->deinitAll();
        }
        CG_CR_LOG_INFO("Freed scene");

    }
}