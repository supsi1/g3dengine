#include "Log.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace CGLib {
    Log* Log::getInstance()
    {
        static Log* instance = nullptr;
        if (instance == nullptr) {
            instance = new Log();
        }

        return instance;
    }

    Log::Log()
    {
        spdlog::set_pattern("%^[%T] %n[%l]: %v%$");
        s_coreLogger = spdlog::stdout_color_mt("CGCORE");
        s_clientLogger = spdlog::stdout_color_mt("CLIENT");

        s_coreLogger->set_level(spdlog::level::trace);
        s_clientLogger->set_level(spdlog::level::trace);
    }


    void Log::setClientLevel(spdlog::level::level_enum level) {
        s_clientLogger->set_level(level);
    }

    void Log::setCoreLevel(spdlog::level::level_enum level) {
        s_coreLogger->set_level(level);
    }
}