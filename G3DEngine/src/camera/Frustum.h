#pragma once

#include "Defs.h"
#include "glm/glm.hpp"
#include <vector>
#include <glm/gtx/matrix_decompose.hpp>

namespace CGLib
{
	class LIB_API Frustum
	{
	private:
		glm::mat4 m_projection;
		glm::mat4 m_modelView;
		bool enabled = true;

	public:

		Frustum() = default;
		Frustum(glm::mat4 projection, glm::mat4 modelView);

		bool isInside(glm::vec3 point) const;

		glm::mat4 getProjection() const;

		glm::mat4 getModelview() const;

		void setProjection(glm::mat4& proj);
		void setModelView(glm::mat4& modelView);

		void setEnabled(bool enabled);

		bool isEnabled() const;
		
	};
}
