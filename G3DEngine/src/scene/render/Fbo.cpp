#include "Fbo.h"
#include "cglib.h"
#include "logging/Log.h"
#include "GL/glew.h"
#include "scene/materials/Texture2D.h"

static int m_oldWidth = 0;
static int m_oldHeight = 0;


void CGLib::Fbo::bindColorTexture(Texture2D* texture)
{
	CG_CR_LOG_TRACE("Binding color texture {} to fbo {}", texture->getTexId(), m_id);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->getTexId(), 0);
	glBindTexture(GL_TEXTURE_2D, texture->getTexId());

	m_x = texture->getWidth();
	m_y = texture->getHeight();
	m_texId = texture->getTexId();

	CG_CR_LOG_TRACE("Texture size {}x{}", m_x, m_y);
}

void CGLib::Fbo::bindDepthBuffer(unsigned int x, unsigned int y)
{
	CG_CR_LOG_TRACE("Binding depth buffer {} to fbo {}", m_dbufferId, m_id);

	m_x = x;
	m_y = y;

	if (m_dbufferId)
		glDeleteRenderbuffers(1, &m_dbufferId);


	glGenRenderbuffers(1, &m_dbufferId);
	glBindRenderbuffer(GL_RENDERBUFFER, m_dbufferId);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, x, y);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_dbufferId);
}


void CGLib::Fbo::bind()
{
	m_oldHeight = CGLib::getCtxStatus()->windowHeight;
	m_oldWidth = CGLib::getCtxStatus()->windowWidth;

	glBindFramebuffer(GL_FRAMEBUFFER, m_id);
	if (m_x == 0 || m_y == 0) return;
	updateProjectionMatrix(m_x, m_y);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
}

void CGLib::Fbo::unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	updateProjectionMatrix(m_oldWidth, m_oldHeight);
}

void CGLib::Fbo::init()
{
	glGenFramebuffers(1, &m_id);
}

void CGLib::Fbo::deinit()
{
	glDeleteFramebuffers(1, &m_id);
}
