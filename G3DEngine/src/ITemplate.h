#pragma once

#include "Defs.h"
namespace CGLib {
    class LIB_API ITemplate {
    public:
        virtual void execute() = 0;
    };
}

