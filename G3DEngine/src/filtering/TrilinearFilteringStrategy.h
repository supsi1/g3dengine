#pragma once

#include "Defs.h"

#include "LinearFilteringStrategy.h"

namespace CGLib {
    class LIB_API TrilinearFilteringStrategy : public LinearFilteringStrategy {
        void execute() override;
    };
}
