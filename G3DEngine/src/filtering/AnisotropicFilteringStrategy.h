#pragma once

#include "Defs.h"

#include "../IStrategy.h"
namespace CGLib {
    class LIB_API AnisotropicFilteringStrategy : public IStrategy {
    private:
        int m_multiplier;
    public:
        void setMultiplier(int value);

        void execute() override;
    };
}
