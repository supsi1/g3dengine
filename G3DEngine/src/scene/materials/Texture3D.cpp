#include "Texture3D.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <scene/materials/TextureManager.h>
#include <FreeImage.h>

namespace CGLib {
	 void Texture3D::load()
	 {
		  bind();

		  // Set params:
		  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		  // Set filters:
		  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        for (int curSide = 0; curSide < 6; curSide++)
        {
				auto image = images[curSide];
				FIBITMAP* fBitmap = FreeImage_Load(FreeImage_GetFileType(image.c_str(), 0), image.c_str());
				if (fBitmap == nullptr) continue;
				int intFormat = GL_RGB;
				GLenum extFormat = GL_BGR;
				if (FreeImage_GetBPP(fBitmap) == 32)
				{
					 intFormat = GL_RGBA;
					 extFormat = GL_BGRA;
				}

				// Fix mirroring:
				//FreeImage_FlipHorizontal(fBitmap);  // Correct mirroring from cube's inside
				//FreeImage_FlipVertical(fBitmap);    // Correct JPG's upside-down


				setBitmap(FreeImage_GetBits(fBitmap), curSide);
				
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + curSide, 0, intFormat, FreeImage_GetWidth(fBitmap), FreeImage_GetHeight(fBitmap), 0, extFormat, GL_UNSIGNED_BYTE, (void*)FreeImage_GetBits(fBitmap));
				FreeImage_Unload(fBitmap);
        }
	 }

	 void Texture3D::setBitmap(unsigned char* bitmap, unsigned int index)
	 {
		  m_bitmap[index] = bitmap;
	 }

	 unsigned char* Texture3D::getBitmap(unsigned int index)
	 {
		  return m_bitmap[index];
	 }
	 void Texture3D::bind()
	 {
		  glBindTexture(GL_TEXTURE_CUBE_MAP, getTexId());
	 }
	 void Texture3D::setImage(std::string path, unsigned int index)
	 {
		  images[index] = path;
	 }



}