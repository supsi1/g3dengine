#pragma once
#include "Defs.h"

namespace CGLib {
    class LIB_API Texture
    {
    private:

        unsigned int m_tex_id;

        unsigned char* m_bitmap = nullptr;

        unsigned int m_width;

        unsigned int m_height;



    public:

        virtual ~Texture();

        void loadToGPU();

        virtual void load() = 0;

        virtual void bind() = 0;

        [[nodiscard]] const unsigned int getTexId() const;

        [[nodiscard]] unsigned char* getBitmap() const;

        [[nodiscard]] unsigned int getHeight() const;

        [[nodiscard]] unsigned int getWidth() const;

        void setBitmap(unsigned char* mBitmap);

        void setWidth(unsigned int mWidth);

        void setHeight(unsigned int mHeight);

        void clean();


    };
}

