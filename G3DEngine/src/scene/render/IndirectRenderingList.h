#pragma once
#pragma region DLL_DEFS

#include <list>
#include "../SceneNode.h"
#include "Defs.h"

namespace CGLib {
    class LIB_API IndirectRenderingList {
    private:
        std::list<std::pair<SceneNode *, glm::mat4>> elements;

    public:
        IndirectRenderingList() : elements() {}

    	void clear();
        void pass(SceneNode* node, glm::mat4 matrix);
        void add(SceneNode* node, glm::mat4 matrix);
        void render(glm::mat4 cameraMatrix);
    };
}
