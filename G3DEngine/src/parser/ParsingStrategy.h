#pragma once
#include <iostream>
#include "../scene/SceneNode.h"
#include "../IStrategy.h"

#include "Defs.h"

namespace CGLib {
	class LIB_API ParsingStrategy : IStrategy {
	protected:
		FILE* m_fp;
		const char* m_filepath;
		SceneNode* m_parsed;
	public:
		virtual void execute() = 0;

		void setInputFile(FILE* fp);
		void setInputFilePath(const char* path);

		SceneNode* getResult() const;
	};
}