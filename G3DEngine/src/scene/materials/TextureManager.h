#pragma once

#include <unordered_map>
#include "Texture.h"
#include "../../filtering/FilteringTemplate.h"

#include "Defs.h"

namespace CGLib {
    class LIB_API TextureManager {
    private:
        std::unordered_map<unsigned int, Texture *> m_texture_map{};

        TextureManager();

        FilteringTemplate *m_filtering = nullptr;
        Texture* m_default = nullptr;

    public:
        static TextureManager &getInstance();

        void addTexture(unsigned int id, Texture *texture);

        void removeTexture(unsigned int id);

        Texture *getTexture(unsigned int id) const;

        void setFilteringTemplate(FilteringTemplate *filteringTemplate);

        FilteringTemplate *getFilteringTemplate() const;

        void forceMassiveReload() const;

        Texture* getDefault();
    };
}