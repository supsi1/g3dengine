#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "transform.h"

namespace CGLib {
    glm::mat4 getScaleMatrix(float scale) {
        return glm::scale(glm::mat4(1.0f), glm::vec3(scale));
    }

    glm::mat4 getTranslationMatrix(float Xunits, float Yunits, float Zunits) {
        return glm::translate(glm::mat4(1.0f), glm::vec3(Xunits, Yunits, Zunits));
    }

    glm::mat4 getRotationMatrixX(float degrees) {
        degrees = glm::radians(degrees);
        return glm::rotate(glm::mat4(1.0f), degrees, glm::vec3(1.0f, 0.0f, 0.0f));

    }

    glm::mat4 getRotationMatrix(glm::vec3 eulerAngles) {
        return getRotationMatrixX(eulerAngles.x)
               * getRotationMatrixY(eulerAngles.y)
               * getRotationMatrixZ(eulerAngles.z);
    }

    glm::mat4 getRotationMatrixY(float degrees) {
        degrees = glm::radians(degrees);
        return glm::rotate(glm::mat4(1.0f), degrees, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    glm::mat4 getRotationMatrixZ(float degrees) {
        degrees = glm::radians(degrees);
        return glm::rotate(glm::mat4(1.0f), degrees, glm::vec3(0.0f, 0.0f, 1.0f));
    }

    glm::mat4 getCameraMatrix(glm::vec3 eye, glm::vec3 center, glm::vec3 up) {
        return glm::lookAt(eye, center, up);
    }

    glm::mat4 getProjectionMatrix(float fov, float aratio, float nearplane, float farplane) {
        fov = glm::radians(fov);
        return glm::perspective(fov, aratio, nearplane, farplane);
    }

    glm::mat4 getInverseMatrix(glm::mat4 matrix) {
        return glm::inverse(matrix);
    }

    glm::mat4 getScaleMatrix(glm::vec3 scale) {
        return glm::scale(glm::mat4(1.0f), scale);
    }

    glm::mat4 getTranslationMatrix(glm::vec3 translate) {
        return glm::translate(glm::mat4(1.0f), translate);
    }
}