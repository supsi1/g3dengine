//
// Created by ubuntu on 30/11/20.
//

#include "LightBuilder.h"

namespace CGLib {
    LightBuilder &LightBuilder::begin() {

        m_translation = glm::vec3{0.0f};
        m_rotation = glm::vec3{0.0f};
        m_scaling = glm::vec3{1.0f};

        m_ambient = glm::vec4{0.0f};
        m_diffuse = glm::vec4{0.5f};
        m_specular = glm::vec4{1.0f};

        m_constant = 1.0f;
        m_linear = 0.0f;
        m_quadratic = 0.0f;

        m_cutoff = 45.0f;
        m_direction = glm::vec3{0.0f, 0.0f, -1.0f};

        return *this;

    }

    LightBuilder &LightBuilder::setTransform(glm::vec3 translate, glm::vec3 rotate, glm::vec3 scale) {

        setTranslation(translate);
        setRotation(rotate);
        setScaling(scale);

        return *this;

    }

    LightBuilder &LightBuilder::setTranslation(glm::vec3 translate) {

        m_translation = translate;

        return *this;

    }

    LightBuilder &LightBuilder::setRotation(glm::vec3 rotate) {

        m_rotation = rotate;

        return *this;

    }

    LightBuilder &LightBuilder::setScaling(glm::vec3 scale) {

        m_scaling = scale;

        return *this;

    }

    LightBuilder &LightBuilder::setLightComponents(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular) {

        setAmbient(ambient);
        setDiffuse(diffuse);
        setSpecular(specular);

        return *this;

    }

    LightBuilder &LightBuilder::setAmbient(glm::vec4 ambient) {

        m_ambient = ambient;

        return *this;

    }

    LightBuilder &LightBuilder::setDiffuse(glm::vec4 diffuse) {

        m_diffuse = diffuse;

        return *this;

    }

    LightBuilder &LightBuilder::setSpecular(glm::vec4 specular) {

        m_specular = specular;

        return *this;

    }

    LightBuilder &LightBuilder::setAttenuationComponents(float constant, float linear, float quadratic) {

        setConstantAttenuation(constant);
        setLinearAttenuation(linear);
        setQuadraticAttenuation(quadratic);

        return *this;

    }

    LightBuilder &LightBuilder::setConstantAttenuation(float constant) {

        m_constant = constant;

        return *this;

    }

    LightBuilder &LightBuilder::setLinearAttenuation(float linear) {

        m_linear = linear;

        return *this;

    }

    LightBuilder &LightBuilder::setQuadraticAttenuation(float quadratic) {

        m_quadratic = quadratic;

        return *this;

    }

    LightBuilder &LightBuilder::setCutoff(float cutoff) {

        m_cutoff = cutoff;

        return *this;

    }

    LightBuilder &LightBuilder::setDirection(glm::vec3 direction) {

        m_direction = direction;

        return *this;

    }

    DirectionalLight *LightBuilder::buildDirectional() {

        return new DirectionalLight{
                0,
                m_translation,
                m_rotation,
                m_scaling,
                m_ambient,
                m_diffuse,
                m_specular,
                m_linear,
                m_constant,
                m_quadratic
        };

    }

    OmniDirectionalLight *LightBuilder::buildOmniDirectional() {

        return new OmniDirectionalLight{
                0,

                m_translation,
                m_rotation,
                m_scaling,
                m_ambient,
                m_diffuse,
                m_specular,
                m_linear,
                m_constant,
                m_quadratic,
        };

    }

    SpotLight *LightBuilder::buildSpot() {

        return new SpotLight{
                0,
                m_translation,
                m_rotation,
                m_scaling,
                m_ambient,
                m_diffuse,
                m_specular,
                m_linear,
                m_constant,
                m_quadratic,
                m_cutoff,
                m_direction
        };

    }
}
