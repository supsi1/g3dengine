//
// Created by kalu on 12/25/20.
//

#include "NoFilteringStrategy.h"
#include <GL/glew.h>
#include <GL/freeglut.h>

namespace CGLib {
    void NoFilteringStrategy::execute() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }
}