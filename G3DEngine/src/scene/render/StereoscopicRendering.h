#pragma once
#include "IRenderingStrategy.h"
#include "IndirectRenderingList.h"
#include "Defs.h"
#include "Fbo.h"
#include "../Plane.h"

namespace CGLib {
	class CameraVR;
	class ShaderProgram;

	class LIB_API StereoscopicRendering : public IRenderingStrategy {

    private:

        IndirectRenderingList *m_list;
        std::vector<Fbo*> m_fbos;
        float m_lensWidth = 200;
        float m_lensHeight = 200;
        ShaderProgram* m_passThroughProgram;
        Plane m_leftPlane{};
        Plane m_rightPlane{};
	
    public:


        explicit StereoscopicRendering(IndirectRenderingList* m_list)
	        : m_list(m_list)
        {
        }

        void render(std::vector<SceneNode*> nodes, Camera* camera) override;

		

        void setLensWidth(float width);

        void setLensHeight(float height);

        void init() override;

        void deinit() override;

    };

}
