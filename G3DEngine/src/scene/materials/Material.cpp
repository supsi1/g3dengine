//
// Created by purpleknight on 11/16/20.
//

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include "scene/SceneObject.h"
#include "scene/LuminousObject.h"
#include "Material.h"
#include "shading/ShaderProgramManager.h"
#include "logging/Log.h"
#include <scene/materials/TextureManager.h>

namespace CGLib {
    Material::Material(const glm::vec4 &diffuse, const glm::vec4 &ambient, const glm::vec4 &specular,
                       const glm::vec4 &emission,
                       float shine, unsigned int orientation)
            : LuminousObject(diffuse, ambient, specular, emission),
              m_shine(shine),
              m_orientation(orientation) {};

    float Material::getShine() const {
        return m_shine;
    }

    void Material::setShine(float pShine) {
        Material::m_shine = pShine;
    }

    unsigned int Material::getOrientation() const {
        return m_orientation;
    }

    void Material::setOrientation(unsigned int orientation) {
        Material::m_orientation = orientation;
    }

    void Material::apply() {

		  if (m_program == nullptr) {
				setProgram(ShaderProgramManager::getInstance().getDefaultProgram());
		  }

        if (m_texture == nullptr) {
            setTexture(TextureManager::getInstance().getDefault());
        }

        m_texture->bind();


		  ShaderProgramManager::getInstance().useProgram(m_program);

        glUniform3fv(
            glGetUniformLocation(m_program->getId(), "matEmission"),
            1,
            glm::value_ptr(getEmission())
        );

        glUniform3fv(
            glGetUniformLocation(m_program->getId(), "matAmbient"),
            1,
            glm::value_ptr(getAmbient())
        );

        glUniform3fv(
            glGetUniformLocation(m_program->getId(), "matDiffuse"),
            1,
            glm::value_ptr(getDiffuse())
        );

        glUniform3fv(
            glGetUniformLocation(m_program->getId(), "matSpecular"),
            1,
            glm::value_ptr(getSpecular())
        );

        glUniform1f(
            glGetUniformLocation(m_program->getId(), "matShininess"),
            getShine()
        );
    }

    Texture *Material::getTexture() const {
        return m_texture;
    }

    void Material::setTexture(Texture *mTexture) {
        m_texture = mTexture;
    }
	 void Material::setProgram(ShaderProgram* program)
	 {
		  m_program = program;
	 }

	 ShaderProgram* Material::getProgram()
	 {
		  return m_program;
	 }
}