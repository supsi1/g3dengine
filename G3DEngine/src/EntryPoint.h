#pragma once

#include "logging/Log.h"
#include "scene/render/IndirectRendering.h"

CGLib::App* app;

extern CGLib::App* CGLib::createCGApp();

void display_callback() {
    CGLib::clear();
    
    CGLib::SceneGraph::getInstance().renderScene(CGLib::CameraManager::getInstance().getActiveCamera());
    
    app->onRender();
    
    CGLib::refreshDisplay();
}

void reshape_callback(int x, int y) {

    CGLib::getCtxStatus()->windowWidth = x;
    CGLib::getCtxStatus()->windowHeight = y;

    CGLib::updateProjectionMatrix(x,y);

    app->onResize(x, y);
}

void keyboard_callback(unsigned char key, int mouseX, int mouseY) {
    app->onKeyboardInput(key, mouseX, mouseY);
}

int main(int argc, char** argv) {
	app = CGLib::createCGApp();

    CGLib::setWindowOptions(CGLIB_RGBA | CGLIB_DOUBLE_BUFFER | CGLIB_DEPTH_BUFFER, 1000, 500);

    CG_LOG_INFO("library setup complete");

    auto* renderingList = new CGLib::IndirectRenderingList();
    auto* renderStrategy = new CGLib::IndirectRendering(renderingList);
    CGLib::SceneGraph::getInstance().setRenderingStrategy(renderStrategy);
    CGLib::init("CGLib", &argc, argv);
    app->onInit();


    CGLib::createWindow();
    CGLib::GUIManager::init();
	
    CGLib::enableLights();
    CGLib::setBackgroundColor(.2f, .2f, 0.3f, 1.0f);

    CGLib::setDisplayCallback(display_callback);
    CGLib::setKeyboardEventCallback(keyboard_callback);
    CGLib::setWindowReshapeCallback(reshape_callback);

    CG_LOG_INFO("library setup complete");

    // Enable Linear + Anisotropic Filtering
    CGLib::FilteringTemplate *filtering = new CGLib::FilteringTemplate();
    CGLib::AnisotropicFilteringStrategy *anisotropicStrategy = new CGLib::AnisotropicFilteringStrategy();
    anisotropicStrategy->setMultiplier(8);

    filtering->setAnisotropicStrategy(anisotropicStrategy);
    filtering->setPerPixelStrategy(new CGLib::LinearFilteringStrategy());

    CGLib::TextureManager::getInstance().setFilteringTemplate(filtering);

    app->onInitComplete();

    CGLib::SceneGraph::getInstance().init();
    CGLib::ShaderProgramManager::getInstance().init();

    CG_LOG_INFO("Entring main loop");
    CGLib::enterMainLoop();

    CGLib::SceneGraph::getInstance().deinit();
    CGLib::ShaderProgramManager::getInstance().deinit();
	CGLib::GUIManager::deinit();

    delete renderingList;
    delete renderStrategy;
}
