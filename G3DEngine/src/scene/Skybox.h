#pragma once
#include "Mesh.h"
#include "Defs.h"
#include "./materials/Texture3D.h"



namespace CGLib {
static char vertShader[] = R"(
   #version 440 core

   uniform mat4 projection;
   uniform mat4 modelview;

   layout(location = 0) in vec3 in_Position;      

   out vec3 texCoord;

   void main(void)
   {
      texCoord = in_Position;
      gl_Position = projection * modelview * vec4(in_Position, 1.0f);            
   }
)";

static char fragShader[] = R"(
   #version 440 core
   
   in vec3 texCoord;
   
   // Texture mapping (cubemap):
   layout(binding = 0) uniform samplerCube cubemapSampler;

   out vec4 fragOutput;

   void main(void)
   {       
      fragOutput =  texture(cubemapSampler, texCoord);
   }
)";

    static const glm::vec3 skyboxVertices[] = {
        glm::vec3{-1, 1, 1},
        glm::vec3{-1, -1, 1},
        glm::vec3{1, -1, 1},
        glm::vec3{1, 1, 1},
        glm::vec3{-1, 1, -1},
        glm::vec3{-1, -1, -1},
        glm::vec3{1, -1, -1},
        glm::vec3{1, 1, -1}
    };

    static const unsigned int skyboxTriangles[] = {
           0, 2, 1,
           0, 3, 2,
           3, 6, 2,
           3, 7, 6,
           4, 3, 0,
           4, 7, 3,
           6, 4, 5,
           7, 4, 6,
           4, 1, 5,
           4, 0, 1,
           1, 6, 5,
           1, 2, 6,
    };

    class LIB_API Skybox : public Mesh
    {
    private:
        std::string m_images[6];
    public:

        Skybox(std::string images[6]) : Mesh(skyboxVertices, skyboxTriangles, 8, 12){
        	bboxMin = glm::vec3{FLT_MAX};
        	bboxMax = glm::vec3{FLT_MIN};
            for(int i=0; i<8; i++)
            {
	            if(skyboxVertices[i].x < bboxMin.x)
	            {
		            bboxMin.x = skyboxVertices[i].x;
	            }
            	if(skyboxVertices[i].y < bboxMin.y)
	            {
		            bboxMin.y = skyboxVertices[i].y;
	            }
            	if(skyboxVertices[i].z < bboxMin.z)
	            {
		            bboxMin.z = skyboxVertices[i].z;
	            }

            	if(skyboxVertices[i].x > bboxMax.x)
	            {
		            bboxMax.x = skyboxVertices[i].x;
	            }
            	if(skyboxVertices[i].y > bboxMax.y)
	            {
		            bboxMax.y = skyboxVertices[i].y;
	            }
            	if(skyboxVertices[i].z > bboxMax.z)
	            {
		            bboxMax.z = skyboxVertices[i].z;
	            }
            }
        	
            for (int i = 0; i < 6; i++) {
                m_images[i] = images[i];
            }
        }
        void render(glm::mat4 camera, glm::mat4 objectMatrix);
        void init();
    };
}
