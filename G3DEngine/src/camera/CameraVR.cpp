#include "CameraVR.h"

#include <glm/gtx/matrix_decompose.hpp>



#include "CameraManager.h"
#include "cglib.h"
#include "scene/render/OvVR.h"

CGLib::CameraVR::~CameraVR()
{
	CGLib::CameraManager::getInstance().remove(m_left);
	CGLib::CameraManager::getInstance().remove(m_right);
	
	delete m_left;
	delete m_right;
}

CGLib::CameraVR::CameraVR(const glm::vec3& rotate, const glm::vec3& translate, const glm::vec3& scale,
                          const glm::vec3& eye, const glm::vec3& center, const glm::vec3& up)
{
	getFrustum().setEnabled(false);
	m_left = new Camera(rotate, translate, scale, eye, center, up);
	m_right = new Camera(rotate, translate, scale, eye, center, up);
	CGLib::CameraManager::getInstance().add(m_left);
	CGLib::CameraManager::getInstance().add(m_right);
}


void CGLib::CameraVR::computeFrustum()
{

	glm::mat4 l_eye2Head = OvVR::getInstance().getEye2HeadMatrix(OvVR::EYE_LEFT);
	glm::mat4 r_eye2Head = OvVR::getInstance().getEye2HeadMatrix(OvVR::EYE_RIGHT);
	glm::mat4 ovrLeftProj = getLeftEyeProjectionMatrix() * glm::inverse(l_eye2Head);
	glm::mat4 ovrRightProj = getRightEyeProjectionMatrix() * glm::inverse(r_eye2Head);

	
	m_left->setFrustum({ ovrLeftProj, getLeftEyeMatrix() });
	m_right->setFrustum({ ovrRightProj, getRightEyeMatrix() });
}

void CGLib::CameraVR::setHead(const glm::vec3& mHead)
{
	m_eye = mHead;
	m_left->setEye(mHead);
	m_right->setEye(mHead);
}

void CGLib::CameraVR::translate(glm::vec3 translation)
{
	Camera::translate(translation);
	m_left->translate(translation);
	m_right->translate(translation);
}

glm::vec3 CGLib::CameraVR::getForward() const
{
	 glm::mat4 transformation = OvVR::getInstance().getModelviewMatrix(); // your transformation matrix.
	 glm::vec3 scale;
	 glm::quat rotation;
	 glm::vec3 translation;
	 glm::vec3 skew;
	 glm::vec4 perspective;
	 glm::decompose(transformation, scale, rotation, translation, skew, perspective);
	glm::vec4 view = rotation * glm::vec4{0,0,-1,1};
	return glm::normalize(glm::vec3{view.x, view.y, view.z});
}

glm::mat4 CGLib::CameraVR::getLeftEyeProjectionMatrix()
{
	auto ovr = OvVR::getInstance();
	return ovr.getProjMatrix(OvVR::EYE_LEFT, CGLib::getCtxStatus()->nearPlane, CGLib::getCtxStatus()->farPlane);
}

glm::mat4 CGLib::CameraVR::getRightEyeProjectionMatrix()
{
	auto ovr = OvVR::getInstance();
	return ovr.getProjMatrix(OvVR::EYE_RIGHT, CGLib::getCtxStatus()->nearPlane, CGLib::getCtxStatus()->farPlane);
}

glm::mat4 CGLib::CameraVR::getLeftEyeMatrix() const
{
	auto headMatrix = OvVR::getInstance().getModelviewMatrix();
	auto eyeToHeadMatrix = OvVR::getInstance().getEye2HeadMatrix(OvVR::EYE_LEFT);
	return  glm::inverse(headMatrix) * eyeToHeadMatrix * m_left->getCamera();
}

glm::mat4 CGLib::CameraVR::getRightEyeMatrix() const
{
	auto headMatrix = OvVR::getInstance().getModelviewMatrix();
	auto eyeToHeadMatrix = OvVR::getInstance().getEye2HeadMatrix(OvVR::EYE_RIGHT);
	return glm::inverse(headMatrix) *eyeToHeadMatrix * m_right->getCamera();
}

const glm::vec3 CGLib::CameraVR::getEye() const
{
	 glm::mat4 transformation = OvVR::getInstance().getModelviewMatrix(); 
	 glm::vec3 scale;
	 glm::quat rotation;
	 glm::vec3 translation;
	 glm::vec3 skew;
	 glm::vec4 perspective;
	 glm::decompose(transformation, scale, rotation, translation, skew, perspective);
	 return translation + m_eye;
}

