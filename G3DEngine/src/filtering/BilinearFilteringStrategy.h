#pragma once

#include "Defs.h"

#include "LinearFilteringStrategy.h"

namespace CGLib {
    class LIB_API BilinearFilteringStrategy: public LinearFilteringStrategy{
    public:
        void execute() override;
    };
}
