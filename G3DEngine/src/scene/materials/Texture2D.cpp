//
// Created by ubuntu on 21/12/20.
//

#include "Texture2D.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <scene/materials/TextureManager.h>


namespace CGLib {
    void Texture2D::load() {

        bind();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        TextureManager instance = TextureManager::getInstance();
        FilteringTemplate *filteringTemplate = instance.getFilteringTemplate();
        filteringTemplate->execute();
       
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, getWidth(), getHeight(), 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, getBitmap());
    }

    void Texture2D::bind()
    {
        glBindTexture(GL_TEXTURE_2D, getTexId());
    }

}




