//
// Created by kalu on 12/25/20.
//

#include "LinearFilteringStrategy.h"
#include <GL/glew.h>
#include <GL/freeglut.h>

namespace CGLib {
    void LinearFilteringStrategy::execute() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
}