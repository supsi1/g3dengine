#include "IndirectRendering.h"

#include "cglib.h"
#include "camera/Camera.h"
#include "camera/CameraManager.h"
#include "GUI/GUIManager.h"
#include "logging/Log.h"

CGLib::IndirectRendering::IndirectRendering(IndirectRenderingList* list) : m_list(list)
{
}

void CGLib::IndirectRendering::render(std::vector<SceneNode*> nodes, Camera* camera)
{
	for (auto& camera : CameraManager::getInstance().getCameras())
	{
		if (camera->isDirty())
		{
			camera->computeFrustum();
		}
	}
	
	for (auto node : nodes)
	{		
		m_list->pass(node, glm::mat4{1.0f});
	}
	m_list->render(camera->getCamera());
	m_list->clear();

	GUIManager::getInstance().drawGUI();
}

void CGLib::IndirectRendering::init()
{
	CG_CR_LOG_INFO("Initializing indirect rendering");
}

void CGLib::IndirectRendering::deinit()
{
	CG_CR_LOG_INFO("Deinitializing indirect rendering");
}
