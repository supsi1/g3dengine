//
// Created by kalu on 12/3/20.
//

#include "SceneNode.h"
#include "EmptyNode.h"
namespace CGLib {
    EmptyNode::EmptyNode(const glm::vec3& rotate,
        const glm::vec3& translate,
        const glm::vec3& scale) : SceneNode(rotate, translate, scale) {
    }

    std::string EmptyNode::getType()
    {
    	return "empty";
    }

    void EmptyNode::render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) {

    }

    void EmptyNode::init()
    {
    }

    void EmptyNode::deinit()
    {
    }


}