//
// Created by ubuntu on 29/11/20.
//

#include "SpotLight.h"
#include <GL/glew.h>
#include <cglib.h>
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>

namespace CGLib {
    void SpotLight::apply() {

       /* glLightfv(getLightRef(), CGLIB_SPOT_CUTOFF, &m_cutoff);
        glLightfv(getLightRef(), CGLIB_SPOT_DIRECTION, glm::value_ptr(getDirection()));*/

        Light::apply();
    }

    SpotLight::SpotLight(unsigned int lightRef,
                         const glm::vec3 &rotate,
                         const glm::vec3 &translate,
                         const glm::vec3 &scale,
                         const glm::vec4 &diffuse,
                         const glm::vec4 &ambient,
                         const glm::vec4 &specular,
                         float linear,
                         float constant,
                         float quadratic,
                         float cutoff,
                         glm::vec3 direction)
            : Light(lightRef, rotate, translate, scale, diffuse, ambient, specular, 1.0f, linear, constant, quadratic),
              m_cutoff(cutoff), m_direction(direction) {}


    float SpotLight::getCutoff() const {
        return m_cutoff;
    }

    glm::vec3 SpotLight::getDirection() const {

        return m_direction;
    }

    void SpotLight::setCutoff(float cutoff) {
        SpotLight::m_cutoff = cutoff;
    }

    void SpotLight::setDirection(const glm::vec3 &m_direction) {
        SpotLight::m_direction = m_direction;
    }
}