#pragma once

#include "IRenderingStrategy.h"
#include "IndirectRenderingList.h"
#include "Defs.h"

namespace CGLib {

	class LIB_API IndirectRendering : public IRenderingStrategy {

	private:

		IndirectRenderingList *m_list;

	public:

		IndirectRendering(IndirectRenderingList* list);

		void render(std::vector<SceneNode*> nodes, Camera *camera) override;

		void init() override;

		void deinit() override;

	};

}