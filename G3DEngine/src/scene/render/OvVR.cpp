#include "OvVR.h"
#include "logging/Log.h"

OvVR::OvVR() : vrSys(nullptr), vrModels(nullptr), vrComp(nullptr)
{
    controllers.clear();
}

OvVR::~OvVR()
{}

OvVR& OvVR::getInstance() {
    static OvVR instance{};
    return instance;
}

bool OvVR::init() {
    vr::EVRInitError error = vr::VRInitError_None;

    // Init VR system:
    vrSys = vr::VR_Init(&error, vr::VRApplication_Scene);
    if (error != vr::VRInitError_None)
    {
        vrSys = nullptr;
        CG_CR_LOG_ERR("Unable to init VR runtime: {}", vr::VR_GetVRInitErrorAsEnglishDescription(error));
        return false;
    }

    // Init render models:
    vrModels = (vr::IVRRenderModels*)vr::VR_GetGenericInterface(vr::IVRRenderModels_Version, &error);
    if (vrModels == nullptr)
    {
        vrSys = nullptr;
        vr::VR_Shutdown();
        CG_CR_LOG_ERR("Unable to get render model interface: {}", vr::VR_GetVRInitErrorAsEnglishDescription(error));
        return false;
    }

    // Initialize the compositor:
    vrComp = vr::VRCompositor();
    if (!vrComp)
    {
        vrModels = nullptr;
        vrSys = nullptr;
        vr::VR_Shutdown();
        CG_CR_LOG_ERR("Unable to get VR compositor {}");
        return false;
    }

    // Tweaks:        
    //vrComp->ShowMirrorWindow();        

    // Init controller tracking:
    controllers.clear();
    for (unsigned int c = 0; c < vr::k_unMaxTrackedDeviceCount; c++)
    {
        if (vrSys->GetTrackedDeviceClass(c) == vr::TrackedDeviceClass_Controller)
        {
            CG_LOG_INFO("Found controller at {}", c);
            Controller* cont = new Controller();
            cont->id = c;

            unsigned int bufferLen = vrSys->GetStringTrackedDeviceProperty(c, vr::Prop_RenderModelName_String, nullptr, 0, nullptr);
            if (bufferLen == 0)
            {
                CG_CR_LOG_ERR("Unable to get controller render model {}");
                delete cont;
                continue;
            }

            std::string result;
            result.resize(bufferLen);
            vrSys->GetStringTrackedDeviceProperty(c, vr::Prop_RenderModelName_String, &result[0], bufferLen, nullptr);
            CG_LOG_INFO("Controller render model: ' {} '", result);
            controllers.push_back(cont);
        }
    }

    // Done:
    return true;
}

bool OvVR::free()
{
    for (unsigned int c = 0; c < controllers.size(); c++)
        delete controllers[c];
    controllers.clear();

    vr::VR_Shutdown();
    vrComp = nullptr;
    vrModels = nullptr;
    vrSys = nullptr;

    // Done:      
    return true;
}

std::string OvVR::getTrackingSysName()
{
    unsigned int bufferLen = vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String, nullptr, 0, nullptr);
    if (bufferLen == 0)
        return std::string();

    std::string result;
    result.resize(bufferLen);
    vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String, &result[0], bufferLen, nullptr);
    return result;
}

bool OvVR::printRenderModels()
{
    for (unsigned int c = 0; c < vrModels->GetRenderModelCount(); c++)
    {
        char buffer[256];
        vrModels->GetRenderModelName(c, buffer, 256);
        std::cout << "   " << c << ") " << buffer << " model" << std::endl;

        for (unsigned int d = 0; d < vrModels->GetComponentCount(buffer); d++)
        {
            char cbuffer[256];
            vrModels->GetComponentName(buffer, d, cbuffer, 256);
            std::cout << "     " << d << ") " << cbuffer << std::endl;
        }
    }

    // Done:
    return true;
}

std::string OvVR::getManufacturerName()
{
    unsigned int bufferLen = vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_ManufacturerName_String, nullptr, 0, nullptr);
    if (bufferLen == 0)
        return std::string();

    std::string result;
    result.resize(bufferLen);
    vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_ManufacturerName_String, &result[0], bufferLen, nullptr);
    return result;
}

std::string OvVR::getModelNumber()
{
    unsigned int bufferLen = vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_ModelNumber_String, nullptr, 0, nullptr);
    if (bufferLen == 0)
        return std::string();

    std::string result;
    result.resize(bufferLen);
    vrSys->GetStringTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_ModelNumber_String, &result[0], bufferLen, nullptr);
    return result;
}

unsigned int OvVR::getHmdIdealHorizRes()
{
    unsigned int result, dummy;
    vrSys->GetRecommendedRenderTargetSize(&result, &dummy);
    return result;
}

unsigned int OvVR::getHmdIdealVertRes()
{
    unsigned int result, dummy;
    vrSys->GetRecommendedRenderTargetSize(&dummy, &result);
    return result;
}

glm::mat4 OvVR::ovr2ogl(const vr::HmdMatrix34_t& matrix)
{
    return glm::mat4(matrix.m[0][0], matrix.m[1][0], matrix.m[2][0], 0.0f,
        matrix.m[0][1], matrix.m[1][1], matrix.m[2][1], 0.0f,
        matrix.m[0][2], matrix.m[1][2], matrix.m[2][2], 0.0f,
        matrix.m[0][3], matrix.m[1][3], matrix.m[2][3], 1.0f);
}

glm::mat4 OvVR::ovr2ogl(const vr::HmdMatrix44_t& matrix)
{
    return glm::mat4(matrix.m[0][0], matrix.m[1][0], matrix.m[2][0], matrix.m[3][0],
        matrix.m[0][1], matrix.m[1][1], matrix.m[2][1], matrix.m[3][1],
        matrix.m[0][2], matrix.m[1][2], matrix.m[2][2], matrix.m[3][2],
        matrix.m[0][3], matrix.m[1][3], matrix.m[2][3], matrix.m[3][3]);
}

bool OvVR::update()
{
    // Main update method:
    vrComp->WaitGetPoses(vrPoses, vr::k_unMaxTrackedDeviceCount, nullptr, 0);

    // If used, update controllers:
    for (auto c : controllers)
    {
        if (vrPoses[c->id].bPoseIsValid)
            c->matrix = ovr2ogl(vrPoses[c->id].mDeviceToAbsoluteTracking);
    }

    // Done:
    return true;
}

glm::mat4 OvVR::getProjMatrix(OvEye eye, float nearPlane, float farPlane)
{
    switch (eye)
    {
    case EYE_LEFT: return ovr2ogl(vrSys->GetProjectionMatrix(vr::Eye_Left, nearPlane, farPlane)); break;
    case EYE_RIGHT: return ovr2ogl(vrSys->GetProjectionMatrix(vr::Eye_Right, nearPlane, farPlane)); break;
    }
    return glm::mat4{ 1 };
}


glm::mat4 OvVR::getEye2HeadMatrix(OvEye eye)
{
    switch (eye)
    {
    case EYE_LEFT: return ovr2ogl(vrSys->GetEyeToHeadTransform(vr::Eye_Left)); break;
    case EYE_RIGHT: return ovr2ogl(vrSys->GetEyeToHeadTransform(vr::Eye_Right)); break;
    default: return glm::mat4(1.0f);
    }
}


glm::mat4 OvVR::getModelviewMatrix()
{
    if (vrPoses[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid == false)
        return glm::mat4(1.0f);

    glm::mat4 headPos = ovr2ogl(vrPoses[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking);
    return headPos;
}


unsigned int OvVR::getNrOfControllers()
{
    return (unsigned int)controllers.size();
}


Controller* OvVR::getController(unsigned int pos) const
{
    if (pos >= controllers.size())
        return nullptr;
    return controllers.at(pos);
}


void OvVR::setReprojection(bool flag)
{
    vrComp->ForceInterleavedReprojectionOn(flag);
}


void OvVR::pass(OvEye eye, unsigned int eyeTexture)
{
    const vr::Texture_t t = { reinterpret_cast<void*>(uintptr_t(eyeTexture)), vr::TextureType_OpenGL, vr::ColorSpace_Linear };
    switch (eye)
    {
    case EYE_LEFT:  vrComp->Submit(vr::Eye_Left, &t); break;
    case EYE_RIGHT: vrComp->Submit(vr::Eye_Right, &t); break;
    }
}


void OvVR::render()
{
    vrComp->PostPresentHandoff();
}