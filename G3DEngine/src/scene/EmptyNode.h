#pragma once

#include "Defs.h"
#include "SceneNode.h"
namespace CGLib {
    class LIB_API EmptyNode : public SceneNode {
    public:
        explicit EmptyNode(const glm::vec3& rotate = glm::vec3(0.0f),
            const glm::vec3& translate = glm::vec3(0.0f),
            const glm::vec3& scale = glm::vec3(1.0f));


        std::string getType() override;
        void render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) override;
        void init() override;
        void deinit() override;
    };
}
