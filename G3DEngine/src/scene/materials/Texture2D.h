#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "Texture.h"
#include "Defs.h"

namespace CGLib {
    class LIB_API Texture2D : public Texture {

    public:
        void load();
        void bind();
    };
}
