#pragma once
#include "Defs.h"

#include "../IStrategy.h"

namespace CGLib {
    class LIB_API NoFilteringStrategy : public IStrategy {
    public:
        void execute() override;
    };
}
