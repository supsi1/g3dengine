#include "App.h"
#include "logging/Log.h"

namespace CGLib {
	App::App() {
		CG_CR_LOG_INFO("User app created");
	}

	App::~App() {
		CG_CR_LOG_INFO("User app destroyed");
	}

	void App::onInit()
	{
		CG_CR_LOG_INFO("calling user init callback");
	}

	void App::onInitComplete()
	{
		CG_CR_LOG_INFO("calling user window callback");
	}

	void App::onRender()
	{
		CG_CR_LOG_TRACE("calling user render callback");
	}

	void App::onClose()
	{
		CG_CR_LOG_INFO("calling user close callback");
	}

	void App::onKeyboardInput(unsigned char key, int mouseX, int mouseY)
	{
	}

	void App::onResize(int width, int height)
	{
	}
}