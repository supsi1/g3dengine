#pragma once

#include "Defs.h"

#include "../ITemplate.h"
#include "../IStrategy.h"

namespace CGLib {
    class LIB_API FilteringTemplate: ITemplate {
    private:
        IStrategy* m_pixelStrategy = nullptr;
        IStrategy* m_anisotropicStrategy = nullptr;
    public:
        void execute() override;
        void setPerPixelStrategy(IStrategy* strategy);
        void setAnisotropicStrategy(IStrategy* strategy);
        IStrategy* getPerPixelStrategy() const;
        IStrategy* getAnisotropicStrategy() const;
        void disableAnisotropicStrategy();
        ~FilteringTemplate();
    };
}
