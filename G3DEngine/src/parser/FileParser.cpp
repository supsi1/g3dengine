#include "FileParser.h"
#include "cglib.h"
#include <logging/Log.h>

namespace CGLib {
    FileParser::FileParser(ParsingStrategy* strategy) : m_strategy(strategy) {
    }

    FileParser::~FileParser() {
        if (m_strategy != nullptr) delete m_strategy;
    }

    void FileParser::setStrategy(ParsingStrategy* strategy) {
        if (m_strategy != nullptr) delete m_strategy;
        this->m_strategy = strategy;
    }

    SceneNode* FileParser::loadFromFile(const std::string& fileName, bool assetsDirRelative) {
        std::string finalPath = fileName;
        if (assetsDirRelative) {
            std::string assetsDir{ CGLib::getAssetsDirPath() };
            finalPath = assetsDir + separator() + fileName;
        }

        CG_CR_LOG_INFO("Parsing: {}", finalPath);

        FILE* in = fopen(finalPath.c_str(), "rb");

        if (in == nullptr) {
            CG_CR_LOG_ERR("Cannnot find {}", finalPath);
            return nullptr;
        }

        m_strategy->setInputFile(in);
        m_strategy->setInputFilePath(finalPath.c_str());

        m_strategy->execute();
        CG_CR_LOG_INFO("Parsed: {}", finalPath);

        return m_strategy->getResult();
    }
}