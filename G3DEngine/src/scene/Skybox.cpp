#include "Skybox.h"
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <shading/ShaderProgramManager.h>
#include <camera/CameraManager.h>

namespace CGLib {
	
    void Skybox::render(glm::mat4 camera, glm::mat4 objectMatrix) {
        glm::vec3 position = CameraManager::getInstance().getActiveCamera()->getEye();
        Mesh::render(camera, getTranslationMatrix(position) * objectMatrix);
    }

    void Skybox::init()
    {
        Texture3D* skyboxTexture = new Texture3D();
        for (int i = 0; i < 6; i++) {
            skyboxTexture->setImage(m_images[i], i);
        }

        skyboxTexture->loadToGPU();

        Material* skyboxMaterial = new Material();
        skyboxMaterial->setTexture(skyboxTexture);
        setMaterial(skyboxMaterial);

        ShaderProgram* program = new ShaderProgram();
        ShaderObject* vertexShader = new ShaderObject(vertShader, ShaderType::VERTEX);
        ShaderObject* fragmentShader = new ShaderObject(fragShader, ShaderType::FRAGMENT);

        program->setVertexShader(vertexShader);
        program->setFragmentShader(fragmentShader);
        program->createProgram();

        ShaderProgramManager::getInstance().addProgram(program);

        m_material->setProgram(program);

        Mesh::init();
    }
}