#pragma once
#include "Mesh.h"
#include "Defs.h"


namespace CGLib
{

	static const glm::vec2 texCoords[] = {
		glm::vec2{0,0},
		glm::vec2{0,1},
		glm::vec2{1,0},
		glm::vec2{1,1},
	};

	static const glm::vec3 vertices[] = {
		glm::vec3{-1, -1, 0}, // lower left
		glm::vec3{-1, 1, 0}, // upper left
		glm::vec3{1, -1, 0}, // lower right
		glm::vec3{1, 1, 0} // upper right
	};

	static const unsigned int triangles[] = {
		0, 2, 3,
		0, 3, 1
	};
	
	class LIB_API Plane : public Mesh
	{
	public:
		Plane() : Mesh(vertices, triangles, 4, 6)
		{
			setMapping(texCoords);
		}

		void render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix) override;
		void init() override;
		void deinit() override;
	};
}
