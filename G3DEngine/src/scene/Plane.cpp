#include "Plane.h"
#include "GL/glew.h"

void CGLib::Plane::render(glm::mat4 cameraMatrix, glm::mat4 objectMatrix)
{
    glBindVertexArray(m_vao);

    glDrawElements(GL_TRIANGLES, m_trianglesNumber * 3, GL_UNSIGNED_INT, nullptr);

    glBindVertexArray(0);
}

void CGLib::Plane::init()
{
    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_verticesVbo);
    glGenBuffers(1, &m_facesVbo);
    glGenBuffers(1, &m_mappingVbo);

    glBindBuffer(GL_ARRAY_BUFFER, m_verticesVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_verticesNumber, m_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_facesVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 3 * m_trianglesNumber, m_triangles, GL_STATIC_DRAW);

	
    glBindBuffer(GL_ARRAY_BUFFER, m_mappingVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * m_verticesNumber, m_mapping, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);
}

void CGLib::Plane::deinit()
{
    glDeleteBuffers(1, &m_verticesVbo);
    glDeleteBuffers(1, &m_mappingVbo);
    glDeleteBuffers(1, &m_facesVbo);
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &m_vao);
}
