#pragma once

#include "Defs.h"

#include <glm/glm.hpp>

namespace CGLib {
    class LIB_API LuminousObject {
    private:
        glm::vec4 m_diffuse, m_ambient, m_specular, m_emission;

    public:

        LuminousObject(
            const glm::vec4& diffuse,
            const glm::vec4& ambient,
            const glm::vec4& specular,
            const glm::vec4& emission = glm::vec4{ 0.0f, 0.0f, 0.0f, 1.0f });

        [[nodiscard]] const glm::vec4& getDiffuse() const;

        void setDiffuse(const glm::vec4& diffuse);

        void setDiffuse(const glm::vec3& diffuse);

        [[nodiscard]] const glm::vec4& getAmbient() const;

        void setAmbient(const glm::vec4& ambient);

        void setAmbient(const glm::vec3& ambient);

        [[nodiscard]] const glm::vec4& getSpecular() const;

        void setSpecular(const glm::vec4& specular);

        void setSpecular(const glm::vec3& specular);

        virtual void apply() = 0;

        const glm::vec4& getEmission() const;

        void setEmission(const glm::vec4& emission);

        void setEmission(const glm::vec3& emission);
    };
}

