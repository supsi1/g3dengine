#include "StereoscopicRendering.h"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtc/type_ptr.inl>
#include "scene/Plane.h"
#include "shading/ShaderProgramManager.h"
#include <glm/gtx/matrix_decompose.hpp>
#include "logging/Log.h"
#include "GL/glew.h"
#include "scene/materials/TextureManager.h"
#include "OvVR.h"
#include "camera/CameraManager.h"
#include "camera/CameraVR.h"
#include "GUI/GUIManager.h"

static char passThorughVertShader[] =
	R"(
   #version 440 core

   // Uniforms:
   uniform mat4 projection;
   uniform mat4 modelview;

   // Attributes:
   layout(location = 0) in vec3 in_Position;
   layout(location = 1) in vec2 in_TexCoord;

   // Varying:
   out vec2 texCoord;

   void main(void)
   {
      gl_Position = projection * modelview * vec4(in_Position, 1.0f);
      texCoord = in_TexCoord;
   }
)";

static char passThorughFragShader[] =
	R"(
   #version 440 core

   in vec2 texCoord;

   uniform vec4 color;

   out vec4 fragOutput;

   // Texture mapping:
   layout(binding = 0) uniform sampler2D texSampler;

   void main(void)
   {
      // Texture element:
      vec4 texel = texture(texSampler, texCoord);

      // Final color:
      fragOutput = texel;
   }
)";

void CGLib::StereoscopicRendering::render(std::vector<SceneNode*> nodes, Camera* camera)
{
	for (auto node : nodes)
	{
		m_list->pass(node, glm::mat4{1.0f});
	}

	OvVR& ovr = OvVR::getInstance();
	ovr.update();

	for (auto& camera : CameraManager::getInstance().getCameras())
	{
		if (camera->isDirty())
		{
			camera->computeFrustum();
		}
	}

	auto* cameraVR = static_cast<CameraVR*>(camera);

	glm::mat4 l_eye2Head = ovr.getEye2HeadMatrix(OvVR::EYE_LEFT); //offset IOD
	glm::mat4 ovrLeftProj = CameraVR::getLeftEyeProjectionMatrix() * glm::inverse(l_eye2Head);
	glm::mat4 leftModelView = cameraVR->getLeftEyeMatrix();

	glm::mat4 r_eye2Head = ovr.getEye2HeadMatrix(OvVR::EYE_RIGHT); //offset IOD
	glm::mat4 ovrRightProj = CameraVR::getRightEyeProjectionMatrix() * glm::inverse(r_eye2Head);
	glm::mat4 rightModelView = cameraVR->getRightEyeMatrix();

	m_fbos[0]->bind();
	clear();
	setProjectionMatrix(ovrLeftProj);
	m_list->render(leftModelView);
	CGLib::GUIManager::getInstance().drawGUI();
	ovr.pass(OvVR::EYE_LEFT, m_fbos[0]->getTextureId());

	m_fbos[1]->bind();
	clear();
	setProjectionMatrix(ovrRightProj);
	m_list->render(rightModelView);
	CGLib::GUIManager::getInstance().drawGUI();

	ovr.pass(OvVR::EYE_RIGHT, m_fbos[1]->getTextureId());
	ovr.render();

	m_list->clear();
	Fbo::unbind();

	ShaderProgramManager::getInstance().useProgram(m_passThroughProgram);
	glm::mat4 ortho = glm::ortho(0.0f, static_cast<float>(OvVR::getInstance().getHmdIdealHorizRes()), 0.0f,
	                             static_cast<float>(OvVR::getInstance().getHmdIdealVertRes()),
	                             CGLib::getCtxStatus()->nearPlane,
	                             CGLib::getCtxStatus()->farPlane);


	int projLoc = glGetUniformLocation(m_passThroughProgram->getId(), "projection");
	int modelViewLoc = glGetUniformLocation(m_passThroughProgram->getId(), "modelview");
	int colorLoc = glGetUniformLocation(m_passThroughProgram->getId(), "color");

	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(ortho));
	glUniformMatrix4fv(modelViewLoc, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));

	glUniform4fv(colorLoc, 1, glm::value_ptr(glm::vec4(1, 0, 0, .3f)));
	glBindTexture(GL_TEXTURE_2D, m_fbos[0]->getTextureId());

	m_leftPlane.render(camera->getCamera(), glm::mat4(1.0f));

	glUniform4fv(colorLoc, 1, glm::value_ptr(glm::vec4(0, 0, 1, .3f)));
	glBindTexture(GL_TEXTURE_2D, m_fbos[1]->getTextureId());
	m_rightPlane.render(camera->getCamera(), glm::mat4(1.0f));

	glBindTexture(GL_TEXTURE_2D, 0);
}

void CGLib::StereoscopicRendering::setLensWidth(float width)
{
	m_lensWidth = width;
}

void CGLib::StereoscopicRendering::setLensHeight(float height)
{
	m_lensHeight = height;
}

void CGLib::StereoscopicRendering::init()
{
	GUIManager::getInstance().setUIScale(3.0f);

	if (!OvVR::getInstance().init())
	{
		CG_CR_LOG_ERR("Unable to init OpenVR\n");
		return;
	}
	CG_CR_LOG_INFO("Manufacturer:{}  ", OvVR::getInstance().getManufacturerName());
	CG_CR_LOG_INFO("Tracking system:{}  ", OvVR::getInstance().getTrackingSysName());
	CG_CR_LOG_INFO("Model number:{}  ", OvVR::getInstance().getModelNumber());
	CG_CR_LOG_INFO("Initializing steroscopic rendering");

	m_fbos.push_back(new Fbo());
	m_fbos.push_back(new Fbo());

	for (auto fbo : m_fbos)
	{
		fbo->init();

		Texture2D* tex = new Texture2D();
		tex->setWidth(OvVR::getInstance().getHmdIdealHorizRes());
		tex->setHeight(OvVR::getInstance().getHmdIdealVertRes());
		tex->loadToGPU();

		fbo->bind();
		fbo->bindColorTexture(tex);
		fbo->bindDepthBuffer(tex->getWidth(), tex->getHeight());

		GLenum errCode = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (errCode != GL_FRAMEBUFFER_COMPLETE)
		{
			CG_CR_LOG_ERR("Frame buffer {} is not complete! errcode {}", fbo->getId(), errCode);
		}
		else
		{
			CG_CR_LOG_TRACE("Frame buffer {} is ready", fbo->getId());
		}
	}

	Fbo::unbind();

	this->m_passThroughProgram = new ShaderProgram();
	auto* fragShader = new ShaderObject(passThorughFragShader, ShaderType::FRAGMENT);
	auto* vertShader = new ShaderObject(passThorughVertShader, ShaderType::VERTEX);
	this->m_passThroughProgram->setFragmentShader(fragShader);
	this->m_passThroughProgram->setVertexShader(vertShader);

	ShaderProgramManager::getInstance().addProgram(this->m_passThroughProgram);

	glm::vec3 leftVertices[] = {
		glm::vec3{0, 0, -1},
		glm::vec3{0, m_lensHeight, -1},
		glm::vec3{m_lensWidth, 0, -1},
		glm::vec3{m_lensWidth, m_lensHeight, -1},
	};

	glm::vec3 rightVertices[] = {
		glm::vec3{m_lensWidth, 0, -1},
		glm::vec3{m_lensWidth, m_lensHeight, -1},
		glm::vec3{m_lensWidth * 2, 0, -1},
		glm::vec3{m_lensWidth * 2, m_lensHeight, -1},
	};

	m_leftPlane.setVertices(leftVertices);
	m_rightPlane.setVertices(rightVertices);

	m_leftPlane.init();
	m_rightPlane.init();
}

void CGLib::StereoscopicRendering::deinit()
{
	CG_CR_LOG_INFO("Deinitializing steroscopic rendering");
}
